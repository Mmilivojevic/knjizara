var id = window.location.search.slice(1).split('&')[0].split('=')[1]; // čitanje vrednosti prvog parametra GET zahteva na ovu stranicu (id-a u ovom slučaju)

$(document).ready(function() {
	


	var formaIzmena = $("form").eq(0)
	var tabelaZaObicanPrikaz = $("table.forma").eq(0)
	
	

	function kom() {
	
		var params = {
			id: id
		}

		console.log(params)
		
		$.get("Komentari/JedanKomentarAdminDetalji", params, function(odgovor) { 
		
			console.log(odgovor)
	
			if (odgovor.status == "ok") {
			
				
				
				var komentar = odgovor.komentar
				
				var date1 = komentar.datumKomentara
				var s = new Date(date1).toLocaleDateString("en-US")
				
				var d = new Date(date1); 
		var NoTimeDate = d.getFullYear()+'/'+(d.getMonth()+1)+'/'+d.getDate();
		b = NoTimeDate.split(' ')[0];
				
				var date2 = new Date(date1);
				var date3 = date2.getDate()  + '/' + date2.getMonth() + '/' +  date2.getFullYear();
				
				console.log(date3);
	
		
				
				formaIzmena.find("input[name=id]").val(komentar.id)
				formaIzmena.find("input[name=tekst]").val(komentar.tekstKomentara)
				formaIzmena.find("input[name=ocena]").val(komentar.ocena)
				formaIzmena.find("input[name=datumKomentara]").val(b)
				formaIzmena.find("input[name=komentarisanaKnjiga]").val(komentar.komentarisanaKnjiga.naziv)
				formaIzmena.find("input[name=autorKomentara]").val(komentar.autorKomentara.korisnickoIme)
				formaIzmena.find("input[name=status]").val(komentar.status)
				
				
				

				
			}
		})
		console.log("GET: " + "Komentari/JedanKomentarAdminDetalji")
		
		
	}
	kom()
	
	formaIzmena.submit(function() {

		var statusInput = formaIzmena.find("input[name=status]")
		var idInput = formaIzmena.find("input[name=id]")
		
		var status= statusInput.val()
		var id = idInput.val()
		
			
		var params = {
			id: id, 
			status: status
			
		}
		console.log(params)
		
		
		$.post("Komentari/EditKomentaraAdmin", params, function(odgovor) {
		
			console.log(odgovor)

			if (odgovor.status == "ok" || odgovor.status == "odbijen") {
			
				window.location.replace("Komentari")
				
			} else if (odgovor.status == "greska") {
			
				pasusGreska.text(odgovor.poruka)
			}
		})
		console.log("POST: Komentari/EditKomentaraAdmin")

		return false
	})
	
	$("body").show() 
	
})

