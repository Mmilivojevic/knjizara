var knjigaId = window.location.search.slice(1).split('&')[0].split('=')[1]; // čitanje vrednosti prvog parametra GET zahteva na ovu stranicu (id-a u ovom slučaju)

$(document).ready(function() {
	

		
	var tabelaKomentari = $("table#komentari")
	var tabela = $("table.komentari1")
	
	
	
	function sviKomentari() {
	
		var params = {
			knjigaId: knjigaId,
		
			
			
		}
		console.log(params)
		
		

		
		$.get("Knjige/SviKomentari", params, function(odgovor) {
			console.log(odgovor)

			if (odgovor.status == "ok") {
			
				tabela.find("tr:gt(1)").remove()
					var komentari = odgovor.komentari	
					for (var it in komentari) { 
					
					
						var date1 = komentari[it].datumKomentara
			
						var s = new Date(date1).toLocaleDateString("en-US")
						
					
						tabela.append( 
						
								'<li ">' + 
								
								'<span >' + "Korisnik: " +  komentari[it].autorKomentara.korisnickoIme +  '</span>' +
								'<br>' +
								'<span >' +   komentari[it].tekstKomentara +  '</span>' +
								'<br>' +
								'<span>' + "Datum komentara: "  +  s +  '</span>' +
								'<br>' +
								'<span >' + "Ocena komentara: " + komentari[it].ocena + ' <i class="em em-star" aria-role="presentation" aria-label="WHITE MEDIUM STAR"></i>' + '</span>' +
								'</li>'
						)
					}
					tabelaKomentari.show();
				} else {
					tabelaKomentari.hide() 
				}
			}
		)
		console.log("GET: " + "Knjige/SviKomentari")
	}


	sviKomentari()
	
	
	$("body").show() // prikazi stanicu nakon sto se preuzmu podaci
})