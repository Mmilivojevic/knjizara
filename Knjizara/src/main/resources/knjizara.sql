drop schema if exists knjizara;
create schema knjizara;
use knjizara;

CREATE TABLE korisnici (
	korisnickoIme VARCHAR(20),
    lozinka VARCHAR(20) NOT NULL,
    eMail VARCHAR(25) not null,
	ime VARCHAR(20) NOT NULL,
	prezime VARCHAR(30) NOT NULL,
    adresa varchar(50) NOT NULL,
    broj VARCHAR(25) NOT NULL,
    datumRegistracije datetime not null,
    datumRodjenja date NOT NULL,
	administrator BOOL DEFAULT false,
	block BOOL DEFAULT false,
    PRIMARY KEY(korisnickoIme)
);
INSERT INTO korisnici (korisnickoIme, lozinka, eMail, ime, prezime, adresa, broj, datumRegistracije, datumRodjenja,administrator,block) VALUES ('bilja', 'bilja', 'bilja@gmail.com', 'Bilja', 'Nesic', 'Ulica nova 5', '5589746',  '2021-01-08 11:00','2000-01-08 ',true,false);
INSERT INTO korisnici (korisnickoIme, lozinka, eMail, ime, prezime, adresa, broj, datumRegistracije, datumRodjenja,administrator,block) VALUES ('dunja', 'dunja', 'dunja@gmail.com', 'Dunja', 'Dunjic', 'Neka izmisljena 7','01236589', '2014-07-16 13:05','2002-07-16' ,false,false);
INSERT INTO korisnici (korisnickoIme, lozinka, eMail, ime, prezime, adresa, broj, datumRegistracije, datumRodjenja,administrator,block) VALUES ('milica', 'milica', 'milica@gmail.com', 'Milica', 'Micic', 'Nemam ideju 8',  '125639', '2000-09-17 20:45','2005-09-17' ,true,false);


create table slika(
slikaId bigint auto_increment,
imagePath varchar(50) not null,
primary key(slikaId)
);


insert into slika(slikaId,imagePath) values(1,'images/serlok.jpg');
insert into slika(slikaId,imagePath) values(4,'images/shut.jpg');
insert into slika(slikaId,imagePath) values(3,'images/twilight.jpg');


create table knjiga(
 id bigint auto_increment,
 naziv varchar(50) not null,
 isbn bigint(13) not null,
 izdavackaKuca varchar(50) not null,
 autori varchar(30) not null,
 godinaIzdavanja int(4) not null,
 kratakOpis varchar(100) not null,

 cena decimal(10,2) not null,
 brojStranica int not null,
 tipPoveza enum('meki','tvrdi') default'tvrdi',
 pismo enum('latinica','cirilica') default'latinica',
 jezik varchar(20) not null,
 	brojKnjiga int NOT NULL DEFAULT 10,
 PRIMARY KEY(id)
);

insert into knjiga(id,naziv,isbn,izdavackaKuca,autori,godinaIzdavanja,kratakOpis,cena,brojStranica,tipPoveza,pismo,jezik) values(1,'Serlok Homs','4444444444444','Sunce','Misa Misic,Pera Peric',1896,'Film o coveku koji istrazuje ubistva',150.00,100,'tvrdi','latinica','srpski');
insert into knjiga(id,naziv,isbn,izdavackaKuca,autori,godinaIzdavanja,kratakOpis,cena,brojStranica,tipPoveza,pismo,jezik) values(2,'Sirom otvorenih ociju','5555555555555','Zavod','Neki strani pisac',1950,'Film o nekoj radnji koju sam skroz zaboravila,ali ima knjiga sigurno',550.00,300,'meki','latinica','engleski');
insert into knjiga(id,naziv,isbn,izdavackaKuca,autori,godinaIzdavanja,kratakOpis,cena,brojStranica,tipPoveza,pismo,jezik) values(3,'Sumrak','6666666666666','Zvezda','Zenski pisac',2015,'Film o vampirima i vukodlacima',700.00,350,'tvrdi','latinica','srpski');



create table knjigaSlika(
knjigaId bigint,
slikaId bigint,

    FOREIGN KEY(knjigaId) REFERENCES knjiga(id)
		ON DELETE CASCADE,
    FOREIGN KEY(slikaId) REFERENCES slika(slikaId)
		ON DELETE CASCADE,
	PRIMARY KEY(knjigaId, slikaId)
);

insert into knjigaSlika(knjigaId,slikaId) values(1,1);
insert into knjigaSlika(knjigaId,slikaId) values(2,4);
insert into knjigaSlika(knjigaId,slikaId) values(3,3);

CREATE TABLE zanrovi (
	id BIGINT AUTO_INCREMENT,
	naziv VARCHAR(25) NOT NULL,
	PRIMARY KEY(id)
);
INSERT INTO zanrovi (id, naziv) VALUES (1, 'fantastika');
INSERT INTO zanrovi (id, naziv) VALUES (2, 'triler');
INSERT INTO zanrovi (id, naziv) VALUES (3, 'ljubavna');
INSERT INTO zanrovi (id, naziv) VALUES (4, 'poezija');

CREATE TABLE knjigaZanr (
    knjigaId BIGINT,
    zanrId BIGINT,
    PRIMARY KEY(knjigaId, zanrId),
    FOREIGN KEY(knjigaId) REFERENCES knjiga(id)
		ON DELETE CASCADE,
    FOREIGN KEY(zanrId) REFERENCES zanrovi(id)
		ON DELETE CASCADE
);
INSERT INTO knjigaZanr (knjigaId, zanrId) VALUES (1, 3);
INSERT INTO knjigaZanr (knjigaId, zanrId) VALUES (2, 2);
INSERT INTO knjigaZanr (knjigaId, zanrId) VALUES (3, 1);


CREATE TABLE komentar(
	komentarId BIGINT AUTO_INCREMENT,
    tekst VARCHAR(999) NOT NULL,
    ocena VARCHAR(30) NOT NULL,
    datumKomentara DATE,
    status BOOL DEFAULT false,
    
    autorKomentara VARCHAR(30) NOT NULL,
	komentarisanaKnjiga BIGINT NOT NULL,

	PRIMARY KEY(komentarId),
    FOREIGN KEY(komentarisanaKnjiga) REFERENCES knjiga(id)
		ON DELETE CASCADE,
	FOREIGN KEY(autorKomentara) REFERENCES korisnici(korisnickoIme)
		ON DELETE CASCADE
);
INSERT INTO komentar(komentarId,tekst,ocena,datumKomentara,status,autorKomentara,komentarisanaKnjiga) VALUES(1,'neki tekst vezan za knjigu',"5",'2021-01-08',true,'dunja',1);
INSERT INTO komentar(komentarId,tekst,ocena,datumKomentara,status,autorKomentara,komentarisanaKnjiga) VALUES(2,'neki tekst vezan za knjigu',"4",'2021-01-08',true,'dunja',2);
INSERT INTO komentar(komentarId,tekst,ocena,datumKomentara,status,autorKomentara,komentarisanaKnjiga) VALUES(3,'neki tekst vezan za knjigu',"3",'2021-01-08',false,'dunja',3);


CREATE TABLE korpa (
	id BIGINT AUTO_INCREMENT,
	knjigaId BIGINT NOT NULL,
    korisnik VARCHAR(30) NOT NULL,
	PRIMARY KEY(id),
    FOREIGN KEY(knjigaId) REFERENCES knjiga(id)
		ON DELETE CASCADE,
	FOREIGN KEY(korisnik) REFERENCES korisnici(korisnickoIme)
		ON DELETE CASCADE
);

INSERT INTO korpa (knjigaId, korisnik) VALUES (1, 'dunja');
INSERT INTO korpa (knjigaId, korisnik) VALUES (2, 'dunja');
INSERT INTO korpa (knjigaId, korisnik) VALUES (3, 'dunja');