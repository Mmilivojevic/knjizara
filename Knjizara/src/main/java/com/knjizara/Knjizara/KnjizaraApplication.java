package com.knjizara.Knjizara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class KnjizaraApplication extends SpringBootServletInitializer {
	
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(KnjizaraApplication.class);
    }
	public static void main(String[] args) {
		SpringApplication.run(KnjizaraApplication.class, args);
	}

}
