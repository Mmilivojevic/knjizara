package com.knjizara.Knjizara.listeners;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.stereotype.Component;


import com.knjizara.Knjizara.controller.KorpaController;
import com.knjizara.Knjizara.model.Knjiga;

@Component
public class InitHttpSessionListener implements HttpSessionListener {

	
	/** kod koji se izvrsava po kreiranju sesije */
	
	public void sessionCreated(HttpSessionEvent event) {
		
		System.out.println("Inicijalizacija sesijee HttpSessionListener...");

		// pri kreiranju sesije inicijalizujemo je ili radimo neke dodatne aktivnosti	
		
		HttpSession session  = event.getSession();
		
		System.out.println("Session id korisnika je "+ session.getId());

		session.setAttribute(KorpaController.IZABRANE_KNJIGE_KORISNIK_KEY, new ArrayList<Knjiga>());

		System.out.println("Uspeh HttpSessionListener!");
	}
	
	
	
	/**  Brisanje sesije */
	
	public void sessionDestroyed(HttpSessionEvent arg0) {
		
		System.out.println("Brisanje SESIJE HttpSessionListener...");
		
		System.out.println("Uspesno HttpSessionListener!");
	}

}
