package com.knjizara.Knjizara.model;



public class Slika {
	private Long id;
	private String imagePath;
	public Slika(Long id, String imagePath) {
		super();
		this.id = id;
		this.imagePath = imagePath;
	}
	public Slika(String imagePath) {
		super();
		this.imagePath = imagePath;
	}
	public Slika() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime*result + ((id == null) ? 0 : id.hashCode());
		return 31 + id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Slika other = (Slika) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Slika [id=" + id + ", imagePath=" + imagePath + "]";
	}
	
	

}
