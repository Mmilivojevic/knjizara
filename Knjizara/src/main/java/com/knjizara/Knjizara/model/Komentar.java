package com.knjizara.Knjizara.model;

import java.util.Date;

public class Komentar {
	private Long id;
	private String tekstKomentara;
	private String ocena;
	private Date datumKomentara;
	private boolean status;
	private Korisnik autorKomentara;
	private Knjiga komentarisanaKnjiga;
	

	
	public Komentar() {
		super();
	}

	
	

	public Komentar(Long id, String tekstKomentara, String ocena, Date datumKomentara, boolean status,
			Korisnik autorKomentara, Knjiga komentarisanaKnjiga) {
		super();
		this.id = id;
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datumKomentara = datumKomentara;
		this.status = status;
		this.autorKomentara = autorKomentara;
		this.komentarisanaKnjiga = komentarisanaKnjiga;
	}




	public Komentar(String tekstKomentara, String ocena, Date datumKomentara, Korisnik autorKomentara,
			Knjiga komentarisanaKnjiga, boolean status) {
		super();
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datumKomentara = datumKomentara;
		this.autorKomentara = autorKomentara;
		this.komentarisanaKnjiga = komentarisanaKnjiga;
		this.status = status;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTekstKomentara() {
		return tekstKomentara;
	}

	public void setTekstKomentara(String tekstKomentara) {
		this.tekstKomentara = tekstKomentara;
	}

	public String getOcena() {
		return ocena;
	}

	public void setOcena(String ocena) {
		this.ocena = ocena;
	}

	public Date getDatumKomentara() {
		return datumKomentara;
	}

	public void setDatumKomentara(Date datumKomentara) {
		this.datumKomentara = datumKomentara;
	}

	public Korisnik getAutorKomentara() {
		return autorKomentara;
	}

	public void setAutorKomentara(Korisnik autorKomentara) {
		this.autorKomentara = autorKomentara;
	}

	public Knjiga getKomentarisanaKnjiga() {
		return komentarisanaKnjiga;
	}

	public void setKomentarisanaKnjiga(Knjiga komentarisanaKnjiga) {
		this.komentarisanaKnjiga = komentarisanaKnjiga;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	

}
