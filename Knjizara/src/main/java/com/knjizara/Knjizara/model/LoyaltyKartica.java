package com.knjizara.Knjizara.model;

public class LoyaltyKartica {
	
	private Long id;
	private String popust;
	private int poeni;
	private boolean status;
	private Korisnik korisnik;
	
	public LoyaltyKartica(Long id, String popust, int poeni, boolean status, Korisnik korisnik) {
		super();
		this.id = id;
		this.popust = popust;
		this.poeni = poeni;
		this.status = status;
		this.korisnik = korisnik;
	}

	public LoyaltyKartica(String popust, int poeni, boolean status, Korisnik korisnik) {
		super();
		this.popust = popust;
		this.poeni = poeni;
		this.status = status;
		this.korisnik = korisnik;
	}

	public LoyaltyKartica() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPopust() {
		return popust;
	}

	public void setPopust(String popust) {
		this.popust = popust;
	}

	public int getPoeni() {
		return poeni;
	}

	public void setPoeni(int poeni) {
		this.poeni = poeni;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	
	
}
