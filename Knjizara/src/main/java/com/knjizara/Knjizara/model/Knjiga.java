package com.knjizara.Knjizara.model;

import java.util.ArrayList;
import java.util.List;


public class Knjiga {
	
	private Long id ;
	private String naziv;
	private Long ISBN;
	private String izdavackaKuca;
	private String autori;
	private int godinaIzdavanja;
	private String kratakOpis;
	
	private List<Slika> imagePath= new ArrayList<Slika>();
	private List<Zanr> zanrovi=new ArrayList<>();
	private double cena;
	private int brojStranica;
	private String tipPoveza="tvrdi";
	private String pismo="latinica";
	private String jezik;
	private Integer brojKnjiga;
	
	
	public Knjiga() {
		super();
	}


	public Knjiga(String naziv, Long ISBN, String izdavackaKuca, String autori, int godinaIzdavanja, String kratakOpis, double cena, int brojStranica, String tipPoveza, String pismo, String jezik) {
		super();
		this.naziv = naziv;
		this.ISBN = ISBN;
		this.izdavackaKuca = izdavackaKuca;
		this.autori = autori;
		this.godinaIzdavanja = godinaIzdavanja;
		this.kratakOpis = kratakOpis;
		this.cena = cena;
		this.brojStranica = brojStranica;
		this.tipPoveza = tipPoveza;
		this.pismo = pismo;
		this.jezik = jezik;
	}
	

	public Knjiga(Long id, String naziv, Long ISBN, String izdavackaKuca, String autori, int godinaIzdavanja,
			String kratakOpis, double cena, int brojStranica, String tipPoveza, String pismo,
			String jezik,Integer brojKnjiga) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.ISBN = ISBN;
		this.izdavackaKuca = izdavackaKuca;
		this.autori = autori;
		this.godinaIzdavanja = godinaIzdavanja;
		this.kratakOpis = kratakOpis;
		this.cena = cena;
		this.brojStranica = brojStranica;
		this.tipPoveza = tipPoveza;
		this.pismo = pismo;
		this.jezik = jezik;
		this.brojKnjiga=brojKnjiga;
	}


	public Knjiga(String naziv, Long iSBN, String izdavackaKuca, String autori, int godinaIzdavanja, String kratakOpis,
			List<Slika> imagePath, List<Zanr> zanrovi, double cena, int brojStranica, String tipPoveza, String pismo,
			String jezik, Integer brojKnjiga) {
		super();
		this.naziv = naziv;
		ISBN = iSBN;
		this.izdavackaKuca = izdavackaKuca;
		this.autori = autori;
		this.godinaIzdavanja = godinaIzdavanja;
		this.kratakOpis = kratakOpis;
		this.imagePath = imagePath;
		this.zanrovi = zanrovi;
		this.cena = cena;
		this.brojStranica = brojStranica;
		this.tipPoveza = tipPoveza;
		this.pismo = pismo;
		this.jezik = jezik;
		this.brojKnjiga = brojKnjiga;
	}


	public Knjiga(Long id, String naziv, Double cena) {
		this.id=id;
		this.naziv=naziv;
		this.cena=cena;
		
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime*result + ((id == null) ? 0 : id.hashCode());
		return 31 + id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Knjiga other = (Knjiga) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

	public Integer getBrojKnjiga() {
		return brojKnjiga;
	}


	public void setBrojKnjiga(Integer brojKnjiga) {
		this.brojKnjiga = brojKnjiga;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public Long getISBN() {
		return ISBN;
	}


	public void setISBN(Long ISBN) {
		this.ISBN = ISBN;
	}


	public String getIzdavackaKuca() {
		return izdavackaKuca;
	}


	public void setIzdavackaKuca(String izdavackaKuca) {
		this.izdavackaKuca = izdavackaKuca;
	}


	public String getAutori() {
		return autori;
	}


	public void setAutori(String autori) {
		this.autori = autori;
	}


	public int getGodinaIzdavanja() {
		return godinaIzdavanja;
	}


	public void setGodinaIzdavanja(int godinaIzdavanja) {
		this.godinaIzdavanja = godinaIzdavanja;
	}


	public String getKratakOpis() {
		return kratakOpis;
	}


	public void setKratakOpis(String kratakOpis) {
		this.kratakOpis = kratakOpis;
	}


	public List<Slika> getImagePath() {
		return imagePath;
	}


	public void setImagePath(List<Slika> imagePath) {
		this.imagePath.clear();
		this.imagePath.addAll(imagePath);
	}


	public List<Zanr> getZanrovi() {
		return zanrovi;
	}


	public void setZanrovi(List<Zanr> zanrovi) {
		this.zanrovi.clear();
		this.zanrovi.addAll(zanrovi);
	}


	public double getCena() {
		return cena;
	}


	public void setCena(double cena) {
		this.cena = cena;
	}


	public int getBrojStranica() {
		return brojStranica;
	}


	public void setBrojStranica(int brojStranica) {
		this.brojStranica = brojStranica;
	}


	public String getTipPoveza() {
		return tipPoveza;
	}


	public void setTipPoveza(String tipPoveza) {
		this.tipPoveza = tipPoveza;
	}


	public String getPismo() {
		return pismo;
	}


	public void setPismo(String pismo) {
		this.pismo = pismo;
	}


	public String getJezik() {
		return jezik;
	}


	public void setJezik(String jezik) {
		this.jezik = jezik;
	}


	@Override
	public String toString() {
		return "Knjiga [id=" + id + ", naziv=" + naziv + ", ISBN=" + ISBN + ", izdavackaKuca=" + izdavackaKuca
				+ ", autori=" + autori + ", godinaIzdavanja=" + godinaIzdavanja + ", kratakOpis=" + kratakOpis
				+ ", imagePath=" + imagePath + ", zanrovi=" + zanrovi + ", cena=" + cena + ", brojStranica="
				+ brojStranica + ", tipPoveza=" + tipPoveza + ", pismo=" + pismo + ", jezik=" + jezik + "]";
	}






	

}
