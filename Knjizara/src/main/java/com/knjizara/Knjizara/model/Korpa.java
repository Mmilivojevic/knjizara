package com.knjizara.Knjizara.model;

public class Korpa {
	
	private Long id;
	private Korisnik korisnik;
	private Knjiga knjiga;
	public Korpa() {
		super();
	}
	public Korpa(Long id, Korisnik korisnik, Knjiga knjiga) {
		super();
		this.id = id;
		this.korisnik = korisnik;
		this.knjiga = knjiga;
	}
	
	
	public Korpa(Korisnik korisnik, Knjiga knjiga) {
		super();
		this.korisnik = korisnik;
		this.knjiga = knjiga;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public Knjiga getKnjiga() {
		return knjiga;
	}
	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}
	

}
