package com.knjizara.Knjizara.model;

import java.sql.Date;
import java.time.LocalDateTime;



public class Korisnik {

    private String korisnickoIme;
	private String lozinka;
	private String eMail;
	private String ime;
	private String prezime;
	private String adresa;
	private String broj;
	private LocalDateTime datumRegistracije;
	private Date datumRodjenja;
	private boolean administrator=false;
	private boolean block=false;
	public Korisnik() {
		super();
	}

	
	public Korisnik(String korisnickoIme, String eMail, LocalDateTime datumRegistracije,
			boolean administrator, boolean block) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.eMail = eMail;
		this.datumRegistracije = datumRegistracije;
		this.administrator = administrator;
		this.block = block;
	}


	public Korisnik(String korisnickoIme, String lozinka, String eMail, String ime, String prezime, String adresa,
			String broj, LocalDateTime datumRegistracije, Date datumRodjenja, boolean administrator) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.eMail = eMail;
		this.ime = ime;
		this.prezime = prezime;
		this.adresa = adresa;
		this.broj = broj;
		this.datumRegistracije = datumRegistracije;
		this.datumRodjenja = datumRodjenja;
		this.administrator = administrator;
	}
	
	
	

	public Korisnik(String korisnickoIme, String lozinka, String eMail, String ime, String prezime, String adresa,
			String broj, LocalDateTime datumRegistracije, Date datumRodjenja, boolean administrator, boolean block) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.eMail = eMail;
		this.ime = ime;
		this.prezime = prezime;
		this.adresa = adresa;
		this.broj = broj;
		this.datumRegistracije = datumRegistracije;
		this.datumRodjenja = datumRodjenja;
		this.administrator = administrator;
		this.block = block;
	}

	public Korisnik(String korisnickoIme, String lozinka, String eMail, String ime, String prezime, String adresa,
			String broj, LocalDateTime datumRegistracije, Date datumRodjenja) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.eMail = eMail;
		this.ime = ime;
		this.prezime = prezime;
		this.adresa = adresa;
		this.broj = broj;
		this.datumRegistracije = datumRegistracije;
		this.datumRodjenja = datumRodjenja;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime*result + ((korisnickoIme == null) ? 0 : korisnickoIme.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Korisnik other = (Korisnik) obj;
		if (korisnickoIme == null) {
			if (other.korisnickoIme != null)
				return false;
		} else if (!korisnickoIme.equals(other.korisnickoIme))
			return false;
		return true;
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public String geteMail() {
		return eMail;
	}
	
	public boolean isBlock() {
		return block;
	}

	public void setBlock(boolean block) {
		this.block = block;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getBroj() {
		return broj;
	}
	public void setBroj(String broj) {
		this.broj = broj;
	}
	public LocalDateTime getDatumRegistracije() {
		return datumRegistracije;
	}
	public void setDatumRegistracije(LocalDateTime datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}
	public boolean isAdministrator() {
		return administrator;
	}
	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}
	
	public Date getDatumRodjenja() {
		return datumRodjenja;
	}
	public void setDatumRodjenja(Date datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	@Override
	public String toString() {
		return "Korisnik [korisnickoIme=" + korisnickoIme + ", lozinka=" + lozinka + ", eMail=" + eMail + ", ime=" + ime
				+ ", prezime=" + prezime + ", adresa=" + adresa + ", broj=" + broj + ", datumRegistracije="
				+ datumRegistracije + ", datumRodjenja=" + datumRodjenja + ", administrator=" + administrator
				+ ", block=" + block + "]";
	}
	

}
