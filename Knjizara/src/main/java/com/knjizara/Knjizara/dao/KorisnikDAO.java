package com.knjizara.Knjizara.dao;

import java.util.List;

import com.knjizara.Knjizara.model.Korisnik;

public interface KorisnikDAO {
	
	
	public Korisnik findOne(String korisnickoIme);
	public Korisnik findOne(String korisnickoIme,String lozinka);
	public List<Korisnik> findAll();
	public void save(Korisnik korisnik);
	public void update(Korisnik korisnik);
	public void delete(String korisnickoIme);
	public void update1(Korisnik korisnik);
	public Korisnik findOne1(String korisnickoIme);
	public Korisnik findBlokiranogKorisnika(String korisnickoIme,String lozinka);
}
