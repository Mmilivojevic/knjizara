package com.knjizara.Knjizara.dao.impl;


import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.knjizara.Knjizara.dao.KorisnikDAO;
import com.knjizara.Knjizara.model.Korisnik;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO{

	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private class KorisnikRowMapper implements RowMapper<Korisnik>{
		public Korisnik mapRow(ResultSet rs,int rowNum) throws SQLException{
			int index=1;
			String korisnickoIme=rs.getString(index++);
			String eMail=rs.getString(index++);
			String ime=rs.getString(index++);
			String prezime=rs.getString(index++);
			String adresa=rs.getString(index++);
			String broj=rs.getString(index++);
			LocalDateTime datumRegistracije = rs.getTimestamp(index++).toLocalDateTime();
			Date datumRodjenja= rs.getDate(index++);
			Boolean administrator=rs.getBoolean(index++);
			boolean block =rs.getBoolean(index++);
			Korisnik korisnik=new Korisnik(korisnickoIme,null,eMail,ime,prezime,adresa,broj,datumRegistracije,datumRodjenja,administrator,block);
			return korisnik;
			
		}
	}
	public Korisnik findOne(String korisnickoIme) {
		try {
			String sql="SELECT korisnickoIme,eMail,ime,prezime,adresa,broj,datumRegistracije,datumRodjenja,administrator,block FROM korisnici WHERE korisnickoIme=?";
			return jdbcTemplate.queryForObject(sql,new KorisnikRowMapper(),korisnickoIme);
		}catch(EmptyResultDataAccessException ex){
			System.out.println("Korisnik nije pronadjen");
			return null;
		}
		
	
	}
	public Korisnik findOne1(String korisnickoIme) {
		try {
			String sql = "SELECT korisnickoIme, eMail, ime, prezime, adresa, broj, datumRegistracije, datumRodjenja, administrator, block "
						+ " FROM korisnici WHERE korisnickoIme = ? and administrator != true ";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme);
		} catch (EmptyResultDataAccessException ex) {
	
			return null;
		}
	}

	public Korisnik findOne(String korisnickoIme, String lozinka) {
		try {
			String sql="SELECT korisnickoIme,eMail,ime,prezime,adresa,broj,datumRegistracije,datumRodjenja,administrator,block"
					+ " FROM korisnici WHERE korisnickoIme=? AND lozinka=?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(),korisnickoIme,lozinka);
		}
		catch(EmptyResultDataAccessException ex){
			System.out.println("Korisnik nije pronadjen");
			return null;
		}
		
	}

	public List<Korisnik> findAll() {
	
			String sql="SELECT korisnickoIme,email,ime,prezime,adresa,broj,datumRegistracije,datumRodjenja,administrator,block FROM korisnici";
			return jdbcTemplate.query(sql,new KorisnikRowMapper());
		
	}

	

	public void save(Korisnik korisnik) {
		String sql="INSERT INTO korisnici(korisnickoIme,lozinka,eMail,ime,prezime,adresa,broj,datumRegistracije,datumRodjenja,administrator) VALUES(?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(sql,korisnik.getKorisnickoIme(),korisnik.getLozinka(),korisnik.geteMail(),korisnik.getIme(),korisnik.getPrezime(),korisnik.getAdresa(),korisnik.getBroj(),korisnik.getDatumRegistracije(),korisnik.getDatumRodjenja(),korisnik.isAdministrator());
	}

	public void update(Korisnik korisnik) {
		
			String sql="UPDATE korisnici SET administrator=? , block=? "+
			" WHERE korisnickoIme = ? ";
			jdbcTemplate.update(sql,korisnik.isAdministrator(),korisnik.isBlock(),korisnik.getKorisnickoIme());
		
	}
	public void update1(Korisnik korisnik) {
		
		String sql="UPDATE korisnici SET block=? "+
		" WHERE korisnickoIme = ? ";
		jdbcTemplate.update(sql,korisnik.isBlock(),korisnik.getKorisnickoIme());
		System.out.println("SQL ZA UPDATE 1"+sql);
}

	public void delete(String korisnickoIme) {
		String sql="DELETE FROM korisnici WHERE korisnickoIme=?";
		jdbcTemplate.update(sql,korisnickoIme);
		
	}
	@Override
	public Korisnik findBlokiranogKorisnika(String korisnickoIme, String lozinka) {
		try {
			String sql="SELECT korisnickoIme,eMail,ime,prezime,adresa,broj,datumRegistracije,datumRodjenja,administrator,block"
					+ " FROM korisnici WHERE korisnickoIme=? AND lozinka=? AND block= false";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(),korisnickoIme,lozinka);
		}
		catch(EmptyResultDataAccessException ex){
			System.out.println("Korisnik nije pronadjen");
			return null;
		}

}
}