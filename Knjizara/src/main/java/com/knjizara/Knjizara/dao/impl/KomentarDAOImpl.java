package com.knjizara.Knjizara.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.knjizara.Knjizara.dao.KnjigaDAO;
import com.knjizara.Knjizara.dao.KomentarDAO;
import com.knjizara.Knjizara.dao.KorisnikDAO;
import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Komentar;
import com.knjizara.Knjizara.model.Korisnik;

@Repository
public class KomentarDAOImpl implements KomentarDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private KnjigaDAO knjigaDAO;
	@Autowired
	private KorisnikDAO korisnikDAO;
	
	
	private class KomentarRowMapper implements RowMapper<Komentar> {

		@Override
		public Komentar mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index = 1;
			
			Long komentarId = rs.getLong(index++);
			String tekst = rs.getString(index++);
			String ocena = rs.getString(index++);
		
			
			Date datumKomentara = rs.getTimestamp(index++);
			Boolean status = rs.getBoolean(index++);

			String korisnickoIme = rs.getString(index++);
			Korisnik autorKomentara = korisnikDAO.findOne(korisnickoIme);
			
			Long knjigaId = rs.getLong(index++);
			Knjiga komentarisanaKnjiga = knjigaDAO.findOne(knjigaId);
			
			
			Komentar komentar =new Komentar(komentarId, tekst,ocena, datumKomentara, status,autorKomentara, komentarisanaKnjiga );
				return komentar;
			
		}

	}
	@Override
	public Komentar findOne(Long id) {
		String sql = 
				"SELECT k.komentarId, k.tekst, k.ocena, k.datumKomentara, k.status, k.autorKomentara , k.komentarisanaKnjiga  FROM komentar k  " + 
				"							LEFT JOIN knjiga knj ON knj.id = k.komentarId " + 
				"    LEFT JOIN korisnici ko ON ko.korisnickoIme = k.autorKomentara "+
				"							WHERE  k.komentarId = ?  " + 
				"							 ORDER BY k.komentarId";
		
		return jdbcTemplate.queryForObject(sql, new KomentarRowMapper(), id);
		
	}

	@Override
	public List<Komentar> findAll(Long knjigaId) {
		
			String sql = 
					"SELECT k.komentarId, k.tekst, k.ocena, k.datumKomentara, k.status, k.autorKomentara,k.komentarisanaKnjiga  FROM komentar k " 
							+"LEFT JOIN knjiga knj ON knj.id = k.komentarisanaKnjiga "
							+"LEFT JOIN korisnici ko ON ko.korisnickoIme = k.autorKomentara "
							+"WHERE  k.komentarisanaKnjiga = ?  AND status != false  " 
							+" ORDER BY k.komentarId";
			return jdbcTemplate.query(sql, new KomentarRowMapper(), knjigaId);

	}

	@Override
	public List<Komentar> findAll() {
		String sql = 
				"SELECT k.komentarId, k.tekst, k.ocena, k.datumKomentara, k.status, k.autorKomentara, k.komentarisanaKnjiga  FROM komentar k " 
						+"LEFT JOIN knjiga knj ON knj.id = k.komentarisanaKnjiga "
						+"LEFT JOIN korisnici ko ON ko.korisnickoIme = k.autorKomentara "
						+" ORDER BY k.komentarId";
		return jdbcTemplate.query(sql, new KomentarRowMapper());

	}

	@Override
	public int save(Komentar komentar) {
	
			
			
			String sql = "INSERT INTO komentar (tekst, ocena, datumKomentara, status, autorKomentara, komentarisanaKnjiga) VALUES (?, ?, ?, ?, ?, ?) ";
			return jdbcTemplate.update(sql, komentar.getTekstKomentara(), komentar.getOcena(), komentar.getDatumKomentara(), komentar.isStatus(),
																						komentar.getAutorKomentara().getKorisnickoIme(),komentar.getKomentarisanaKnjiga().getId());

	}

	@Override
	public int update(Komentar komentar) {
		String sql = "UPDATE komentar  SET status = ? "
				+ " WHERE komentarId  = ?";
		return jdbcTemplate.update(sql, komentar.isStatus(), komentar.getId());

	}
	
	
	

}
