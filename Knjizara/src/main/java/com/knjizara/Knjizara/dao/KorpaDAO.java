package com.knjizara.Knjizara.dao;

import java.util.List;

import com.knjizara.Knjizara.model.Korpa;


public interface KorpaDAO {

	public Korpa findOne(Long korpaId);
	
	public List<Korpa> findOne(String korisnickoIme, Long knjigaId);
	
	public List<Korpa> findAll();
	
	public List<Korpa> findAll(String korisnickoIme);
	
	public List<Korpa> findAll2(String knjigaId);
	
	public int save(Korpa korpa);
	
	public int delete(Long korpaId);


}
