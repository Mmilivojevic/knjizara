package com.knjizara.Knjizara.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


import com.knjizara.Knjizara.dao.KnjigaDAO;
import com.knjizara.Knjizara.dao.KorisnikDAO;
import com.knjizara.Knjizara.dao.KorpaDAO;
import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Korisnik;
import com.knjizara.Knjizara.model.Korpa;


@Repository
public class KorpaDAOImpl implements KorpaDAO{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KnjigaDAO knjigaDAO;
	
	@Autowired
	private KorisnikDAO korisnikDAO;
	
	private class KorpaRowMapper implements RowMapper<Korpa> {

		@Override
		public Korpa mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index = 1;
			
			Long korpaId = rs.getLong(index++);
			
			Long knjigaId = rs.getLong(index++);
			
			Knjiga knjiga = knjigaDAO.findOne(knjigaId);
			
			String korisnickoIme = rs.getString(index++);

			Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
			
			Korpa korpa = new Korpa(korpaId, korisnik, knjiga);
			return korpa;
			
		}

	}


	@Override
	public Korpa findOne(Long korpaId) {
		String sql = 
				"SELECT k.id, k.knjigaId, k.korisnik  FROM korpa k " 
						+"LEFT JOIN knjiga knj ON knj.id = k.knjigaId "
						+"LEFT JOIN korisnici ko  ON ko.korisnickoIme = k.korisnik "
						+"WHERE k.id = ? " 
						+" ORDER BY k.id";
		
		return jdbcTemplate.queryForObject(sql, new KorpaRowMapper(), korpaId);
		
	}

	@Override
	public List<Korpa> findOne(String korisnickoIme, Long knjigaId) {
		String sql = 
				"SELECT k.id, knj.id, ko.korisnickoIme FROM korpa k " 
						+"LEFT JOIN knjiga knj ON knj.id = k.knjigaId "
						+"LEFT JOIN korisnici ko ON ko.korisnickoIme = k.korisnik "
						+"WHERE korisnik = ? and knjigaId = ? " 
						+" ORDER BY k.id";
				
		return jdbcTemplate.query(sql, new KorpaRowMapper(), korisnickoIme, knjigaId);

	}

	@Override
	public List<Korpa> findAll() {
		String sql = 
				"SELECT k.id, knj.id, ko.korisnickoIme FROM korpa k " 
				+"LEFT JOIN knjiga knj ON knj.id = k.knjigaId "
				+"LEFT JOIN korisnici ko ON ko.korisnickoIme = k.korisnik "
				+" ORDER BY k.id";
		return jdbcTemplate.query(sql, new KorpaRowMapper());
	}

	@Override
	public List<Korpa> findAll(String korisnickoIme) {
		
			String sql = 
					"SELECT k.id, knj.id, ko.korisnickoIme FROM korpa k " 
					+"LEFT JOIN knjiga knj ON knj.id = k.knjigaId "
					+"LEFT JOIN korisnici ko ON ko.korisnickoIme = k.korisnik "
					+"WHERE korisnik = ?  AND administrator != true   " 
					+" ORDER BY k.id";
			return jdbcTemplate.query(sql, new KorpaRowMapper(), korisnickoIme);
		}
	

	@Override
	public List<Korpa> findAll2(String knjigaId) {
		String sql = 
				"SELECT k.id, knj.id, ko.korisnickoIme FROM korpa k " 
						+"LEFT JOIN knjiga knj ON knj.id = k.knjigaId "
						+"LEFT JOIN korisnici ko ON ko.korisnickoIme = k.korisnik  "
						+"WHERE k.knjigaId != ? " 
						+" ORDER BY k.id";
				
	
		
		return jdbcTemplate.query(sql, new KorpaRowMapper(), knjigaId);
		
	}

	@Override
	public int save(Korpa korpa) {

		String sql = "INSERT INTO korpa (knjigaId, korisnik) VALUES (?, ?) ";
		return jdbcTemplate.update(sql, korpa.getKnjiga().getId(), korpa.getKorisnik().getKorisnickoIme());

	}

	@Override
	public int delete(Long korpaId) {
		String sql = "DELETE FROM korpa WHERE id = ?";
		return jdbcTemplate.update(sql, korpaId);
	}
}
