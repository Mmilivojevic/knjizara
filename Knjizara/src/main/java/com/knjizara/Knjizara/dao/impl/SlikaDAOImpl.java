package com.knjizara.Knjizara.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.knjizara.Knjizara.dao.SlikaDAO;
import com.knjizara.Knjizara.model.Slika;

@Repository
@Primary

public class SlikaDAOImpl implements SlikaDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class SlikaRowMapper implements RowMapper<Slika>{

		public Slika mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index=1;
			Long id=rs.getLong(index++);
			String imagePath=rs.getString(index++);
			
			Slika slika=new Slika(id,imagePath);
			return slika;
			
		}
		
	}
	public Slika findOne(Long id) {
		String sql="Select slikaid,imagePath from slika where slikaid=?";
		return jdbcTemplate.queryForObject(sql,new SlikaRowMapper(),id);
	}

	public List<Slika> findAll() {
		String sql="select slikaid,imagePath from slika";
		
		return jdbcTemplate.query(sql,new SlikaRowMapper());
	}

	public List<Slika> find(String naziv) {
		naziv="%"+naziv+"%";
		String sql="Select slikaid,imagePath from slika where imagePath like ?";
		return jdbcTemplate.query(sql,new SlikaRowMapper(),naziv);
		
	}

	public int save(Slika slika) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int[] save(ArrayList<Slika> slike) {
		// TODO Auto-generated method stub
		return null;
	}

	public int update(Slika slika) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int delete(Long id) {
		// TODO Auto-generated method stub
		return 0;
	}

}
