package com.knjizara.Knjizara.dao;

import java.util.List;

import com.knjizara.Knjizara.model.Komentar;



public interface KomentarDAO {
	
	public Komentar findOne(Long id);
	
	public List<Komentar> findAll(Long knjigaId);
	
	public List<Komentar> findAll();
	
	public int save(Komentar komentar);
	
	public int update(Komentar komentar);

}
