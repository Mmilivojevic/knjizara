package com.knjizara.Knjizara.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


import com.knjizara.Knjizara.dao.ZanrDAO;
import com.knjizara.Knjizara.model.Zanr;

@Repository
@Primary
public class ZanrDAOImpl implements ZanrDAO{
	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	private class ZanrRowMapper implements RowMapper<Zanr> {

		@Override
		public Zanr mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String naziv = rs.getString(index++);

			Zanr zanr = new Zanr(id, naziv);
			return zanr;
		}
	}


	@Override
	public Zanr findOne(Long id) {
		String sql = "SELECT id, naziv FROM zanrovi WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new ZanrRowMapper(), id);
	}


	@Override
	public List<Zanr> findAll() {
		String sql = "SELECT id, naziv FROM zanrovi";
		return jdbcTemplate.query(sql, new ZanrRowMapper());
	}


	@Override
	public List<Zanr> find(String naziv) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int save(Zanr zanr) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int[] save(ArrayList<Zanr> zanrovi) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int update(Zanr zanr) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return 0;
	}
}
