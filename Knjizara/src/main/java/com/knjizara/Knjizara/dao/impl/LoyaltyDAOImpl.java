package com.knjizara.Knjizara.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.knjizara.Knjizara.dao.KnjigaDAO;
import com.knjizara.Knjizara.dao.KorisnikDAO;
import com.knjizara.Knjizara.dao.LoyaltyDAO;
import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Komentar;
import com.knjizara.Knjizara.model.Korisnik;
import com.knjizara.Knjizara.model.LoyaltyKartica;

@Repository
public class LoyaltyDAOImpl  implements LoyaltyDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	@Autowired
	private KorisnikDAO korisnikDAO;
	
	private class LoyaltyRowMapper implements RowMapper<LoyaltyKartica> {

		@Override
		public LoyaltyKartica mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index = 1;
			
			Long id = rs.getLong(index++);
			String popust = rs.getString(index++);
			Integer poeni = rs.getInt(index++);
	
			Boolean status = rs.getBoolean(index++);

			String korisnickoIme = rs.getString(index++);
			Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
			

			
			LoyaltyKartica loyaltyKartica =new LoyaltyKartica(id, popust, poeni, status, korisnik);
				return loyaltyKartica;
			
		}

	}

	@Override
	public LoyaltyKartica findOne(Long id) {
		String sql = 
				"SELECT l.id,l.popust, l.poeni, l.status , l.korisnik  FROM loyalty l  " + 
				"    LEFT JOIN korisnici ko ON ko.korisnickoIme = l.korisnik "+
				"							WHERE  l.id = ?  " + 
				"							 ORDER BY l,id ";
		
		return jdbcTemplate.queryForObject(sql, new LoyaltyRowMapper(), id);

	} 

	@Override
	public List<LoyaltyKartica> findAll(String korisnickoIme) {

		String sql = 
				"l.id,l.popust, l.poeni, l.status , l.korisnik  FROM loyalty l" 
						+"LEFT JOIN korisnici ko ON ko.korisnickoIme = l.korisnik "
						+"WHERE  l.korisnik = ?  AND status != false  " 
						+" ORDER BY l.id";
		return jdbcTemplate.query(sql, new LoyaltyRowMapper(), korisnickoIme);

	}

	@Override
	public List<LoyaltyKartica> findAll() {
		String sql = 
				"SELECT i.id, l.popust, l.poeni, l.status, l.korisnik FROM loyalty l " 
						+"LEFT JOIN korisnici ko ON ko.korisnickoIme= l.korisnik "
						+" ORDER BY l.id";
		return jdbcTemplate.query(sql, new LoyaltyRowMapper());

	}

	@Override
	public int saveLoyaltyKartica(LoyaltyKartica loyaltyKartica) {
		String sql="INSERT INTO loyalty (popust, poeni, status, korisnik) VALUES (?,?,?,?)";
		return jdbcTemplate.update(sql, loyaltyKartica.getPopust(), loyaltyKartica.getPoeni(), loyaltyKartica.isStatus(), loyaltyKartica.getKorisnik().getKorisnickoIme());
	}
	

	@Override
	public int updateLoyaltyKartica(LoyaltyKartica loyaltyKartica) {
		String sql= "UPDATE loyalty SET status= ?"
				+"WHERE id=?";
		return jdbcTemplate.update(sql, loyaltyKartica.getId());
		
	}

}
