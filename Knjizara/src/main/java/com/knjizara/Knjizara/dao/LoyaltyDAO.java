package com.knjizara.Knjizara.dao;

import java.util.List;

import com.knjizara.Knjizara.model.LoyaltyKartica;

public interface LoyaltyDAO {
	LoyaltyKartica findOne(Long id);
	List<LoyaltyKartica> findAll(String korisnickoIme);
	List<LoyaltyKartica> findAll();
	public int saveLoyaltyKartica(LoyaltyKartica loyaltyKartica);
	public int updateLoyaltyKartica(LoyaltyKartica loyaltyKartica);

}
