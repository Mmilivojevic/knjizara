package com.knjizara.Knjizara.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.knjizara.Knjizara.dao.KnjigaDAO;
import com.knjizara.Knjizara.dao.ZanrDAO;
import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Slika;
import com.knjizara.Knjizara.model.Zanr;


@Repository
public class KnjigaDAOImpl implements KnjigaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ZanrDAO zanrDAO;
	
	
	
	
	
	
	private class KnjigaSlikaRowCallBackHandler implements RowCallbackHandler{
		
		private Map<Long,Knjiga> knjige=new LinkedHashMap<Long, Knjiga>();
		public void processRow(ResultSet resultSet) throws SQLException {
			int index=1;
			
			Long knjigaId=resultSet.getLong(index++);
			String naziv=resultSet.getString(index++);
			Long isbn=resultSet.getLong(index++);
			String izdavackaKuca=resultSet.getString(index++);
			String autori=resultSet.getString(index++);
			Integer godinaIzdavanja=resultSet.getInt(index++);
			String kratakOpis=resultSet.getString(index++);
			Double cena=resultSet.getDouble(index++);
			Integer brojStranica=resultSet.getInt(index++);
			String tipPoveza=resultSet.getString(index++);
			String pismo=resultSet.getString(index++);
			String jezik=resultSet.getString(index++);
			Integer brojKnjiga=resultSet.getInt(index++);
			Knjiga knjiga=knjige.get(knjigaId);
			if(knjiga==null) {
				knjiga=new Knjiga(knjigaId,naziv,isbn,izdavackaKuca,autori,godinaIzdavanja,kratakOpis,cena,brojStranica,tipPoveza,pismo,jezik,brojKnjiga);
				knjige.put(knjiga.getId(), knjiga);
				
			}
			Long slikaId=resultSet.getLong(index++);
			String imagePath=resultSet.getString(index++);
			Slika slika =new Slika(slikaId,imagePath);
			knjiga.getImagePath().add(slika);
			
			Long zanrId=resultSet.getLong(index++);
			String zanrNaziv=resultSet.getString(index++);
			Zanr zanr=new Zanr(zanrId,zanrNaziv);
			knjiga.getZanrovi().add(zanr);
		}
		public List<Knjiga> getKnjige(){
			return new ArrayList<Knjiga>(knjige.values());
		}
	}
	public Knjiga findOne(Long id) {
		String sql="SELECT k.id,k.naziv,k.isbn,k.izdavackaKuca,k.autori,k.godinaIzdavanja,k.kratakOpis,k.cena,k.brojStranica,k.tipPoveza,k.pismo,k.jezik,k.brojKnjiga,s.slikaid,s.imagePath,z.id,z.naziv FROM knjiga k "+
				"LEFT JOIN knjigaSlika ks ON ks.knjigaId=k.id "+
				"LEFT JOIN slika s ON ks.slikaId=s.slikaId "+
				"LEFT JOIN knjigaZanr kz ON kz.knjigaId = k.id " + 
				"LEFT JOIN zanrovi z ON kz.zanrId = z.id " + 
				"WHERE k.id=? AND k.brojKnjiga > 0 "+
				"ORDER BY k.id";
		KnjigaSlikaRowCallBackHandler rowCallbackHandler=new KnjigaSlikaRowCallBackHandler();
		
		jdbcTemplate.query(sql,rowCallbackHandler,id);
		return rowCallbackHandler.getKnjige().get(0);
	}

	public List<Knjiga> findAll() {
		String sql=
				"SELECT k.id, k.naziv, k.isbn, k.izdavackaKuca, k.autori, k.godinaIzdavanja, k.kratakOpis, k.cena, k.brojStranica, k.tipPoveza, k.pismo, k.jezik,k.brojKnjiga,s.slikaid,s.imagePath,z.id,z.naziv FROM knjiga k "+
				"LEFT JOIN knjigaslika ks on ks.knjigaId = k.id "+
				"LEFT JOIN slika s on ks.slikaId = s.slikaid "+
				"LEFT JOIN knjigaZanr kz ON kz.knjigaId = k.id " + 
				"LEFT JOIN zanrovi z ON kz.zanrId = z.id " + 
				"ORDER BY k.id";
		
		KnjigaSlikaRowCallBackHandler rowCallBackHandler=new KnjigaSlikaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		System.out.println("Sql upit"+sql);
		return rowCallBackHandler.getKnjige();
	
	}
	@Transactional
	//ovde sam stavila final,jer se bunilo da ne moze bey toga
	public int save(final Knjiga knjiga) {
		PreparedStatementCreator preparedStatementCreator=new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql="Insert into knjiga(naziv,isbn,izdavackaKuca,autori,godinaIzdavanja,kratakOpis,cena,brojStranica,tipPoveza,pismo,jezik) values(?,?,?,?,?,?,?,?,?,?,?)";
				
				PreparedStatement preparedStatement =connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				int index=1;
				preparedStatement.setString(index++, knjiga.getNaziv());
				preparedStatement.setLong(index++, knjiga.getISBN());
				preparedStatement.setString(index++, knjiga.getIzdavackaKuca());
				preparedStatement.setString(index++, knjiga.getAutori());
				preparedStatement.setInt(index++, knjiga.getGodinaIzdavanja());
				preparedStatement.setString(index++, knjiga.getKratakOpis());
				preparedStatement.setDouble(index++, knjiga.getCena());
				preparedStatement.setInt(index++, knjiga.getBrojStranica());
				preparedStatement.setString(index++, knjiga.getTipPoveza());
				preparedStatement.setString(index++, knjiga.getPismo());
				preparedStatement.setString(index++, knjiga.getJezik());
				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder=new GeneratedKeyHolder();
		boolean uspeh=jdbcTemplate.update(preparedStatementCreator,keyHolder)==1;
		
		if(uspeh) {
			String sql="Insert into knjigaslika(knjigaId,slikaId) values(?,? )";
			for(Slika itSlika:knjiga.getImagePath()) {
				uspeh=uspeh && jdbcTemplate.update(sql, keyHolder.getKey(),itSlika.getId())==1;
			}
		if(uspeh) {
			String sql2="Insert into knjigazanr(knjigaId,zanrId) values(?,?)";
			for(Zanr itZanr:knjiga.getZanrovi()) {
				uspeh=uspeh&&jdbcTemplate.update(sql2,keyHolder.getKey(),itZanr.getId())==1;
			}
		}
		}
		return uspeh?1:0;
		
	}
	@Transactional
	public int update(Knjiga knjiga) {
		String sql="Delete from knjigaslika where knjigaId=?";
		 jdbcTemplate.update(sql,knjiga.getId());
		 
		 String sql1 = "DELETE FROM knjigazanr WHERE knjigaId = ?";
			jdbcTemplate.update(sql1, knjiga.getId());
		 
		boolean uspeh=true;
		sql1="insert into knjigaslika (knjigaId,slikaId) values(?,?)";
		for(Slika itSlika:knjiga.getImagePath()) {
			uspeh=uspeh && jdbcTemplate.update(sql1,knjiga.getId(),itSlika.getId())==1;
		}
		sql1="insert into knjigazanr (knjigaId, zanrId) values(?,?)";
		for(Zanr itZanr:knjiga.getZanrovi()) {
			uspeh=uspeh && jdbcTemplate.update(sql1,knjiga.getId(),itZanr.getId())==1;
		}

		sql1="UPDATE knjiga SET naziv=?, isbn=?, izdavackaKuca=?, autori=?, godinaIzdavanja=?, kratakOpis=?, cena=?, brojStranica=?, tipPoveza=?, pismo=?, jezik=? , brojKnjiga=brojKnjiga + ?  where id=?";
		System.out.println("SQL za update:"+sql1);
		uspeh=uspeh && jdbcTemplate.update(sql1,knjiga.getNaziv(),knjiga.getISBN(),knjiga.getIzdavackaKuca(),knjiga.getAutori(),knjiga.getGodinaIzdavanja(),knjiga.getKratakOpis(),knjiga.getCena(),knjiga.getBrojStranica(),knjiga.getTipPoveza(),knjiga.getPismo(),knjiga.getJezik(),knjiga.getBrojKnjiga(),knjiga.getId())==1;
		return uspeh?1:0;
	}
	@Transactional
	public int delete(Long id) {
		String sql="delete from knjigaslika where knjigaId=?";
		jdbcTemplate.update(sql,id);
		
		String sql2="delete from knjigazanr where knjigaId=?";
		jdbcTemplate.update(sql2,id);
		
		sql="Delete from knjiga where id=?";
		return jdbcTemplate.update(sql,id);
	}
	
	private class KnjigaRowMapper implements RowMapper<Knjiga>{

		public Knjiga mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index=1;
			Long id=rs.getLong(index++);
			String naziv=rs.getString(index++);
			Double cena=rs.getDouble(index++);
			Long slikaId=rs.getLong(index++);
			String imagePath=rs.getString(index++);
			Slika slika =new Slika(slikaId,imagePath);
			
			Knjiga knjiga=new Knjiga(id,naziv,cena);
			knjiga.getImagePath().add(slika);
			return knjiga;
			
		}
		
	}

	
	@SuppressWarnings({ "deprecation"})
	public List<Knjiga> find(String naziv,Double cenaOd,Double cenaDo) {
		ArrayList<Object> listaArgumenata=new ArrayList<Object>();
		String sql="Select id,naziv,cena,s.slikaid,s.imagePath from knjiga k "+
		
		"LEFT JOIN knjigaslika ks on ks.knjigaid = k.id "+
		"LEFT JOIN slika s on ks.slikaId = s.slikaId "+
		"ORDER BY k.id ";
		
		StringBuffer whereSql=new StringBuffer(" WHERE ");
		boolean imaArgumenata=false;
		
		if(naziv!=null) {
			naziv="%"+naziv+"%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append(" naziv  LIKE ?");
			imaArgumenata=true;
			listaArgumenata.add(naziv);
		}
		
		if(cenaOd!=null) {
			
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append(" cena  >= ?");
			imaArgumenata=true;
			listaArgumenata.add(cenaOd);
		}
			if(cenaDo!=null) {
			
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append(" cena  <= ?");
			imaArgumenata=true;
			listaArgumenata.add(cenaDo);
		}
			
				System.out.println("Lista argumenata"+listaArgumenata);
//			if(imaArgumenata)
//				sql=sql+whereSql.toString()+" ORDER BY k.id ";
//			else
//				sql=sql+"ORDER BY k.id";
			System.out.println(imaArgumenata);
			
	
		List<Knjiga> knjige=jdbcTemplate.query(sql,listaArgumenata.toArray(),new KnjigaRowMapper());
	
		
		
		System.out.println("Knjige po pretrazi  "+listaArgumenata);
		return knjige;
		
	}
	
	private class KnjigaZanrRowMapper implements RowMapper<Long []> {

		@Override
		public Long [] mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long knjigaId = rs.getLong(index++);
			Long zanrId = rs.getLong(index++);

			Long [] knjigaZanr = {knjigaId, zanrId};
			return knjigaZanr;
		}
	}
//	private List<Zanr> findKnjigaZanr(Long knjigaId, Long zanrId) {
//		
//		List<Zanr> zanroviKnjige = new ArrayList<Zanr>();
//		
//		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
//		
//		String sql = 
//				"SELECT kz.knjigaId, kz.zanrId FROM knjigazanr kz ";
//		
//		StringBuffer whereSql = new StringBuffer(" WHERE ");
//		boolean imaArgumenata = false;
//		
//		if(knjigaId!=null) {
//			if(imaArgumenata)
//				whereSql.append(" AND ");
//			whereSql.append("kz.knjigaId = ?");
//			imaArgumenata = true;
//			listaArgumenata.add(knjigaId);
//		}
//		
//		if(zanrId!=null) {
//			if(imaArgumenata)
//				whereSql.append(" AND ");
//			whereSql.append("kz.zanrId = ?");
//			imaArgumenata = true;
//			listaArgumenata.add(zanrId);
//		}
//
//		if(imaArgumenata)
//			sql=sql + whereSql.toString()+" ORDER BY kz.knjigaId";
//		else
//			sql=sql + " ORDER BY kz.knjigaId";
//		System.out.println(sql);
//		
//		List<Long[]> knjigaZanrovi = jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaZanrRowMapper()); 
//				
//		for (Long[] kz : knjigaZanrovi) {
//			zanroviKnjige.add(zanrDAO.findOne(kz[1]));
//		}
//		return zanroviKnjige;
//	}




}
