package com.knjizara.Knjizara.dao;

import java.util.List;

import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Korisnik;

public interface KnjigaDAO {
	
	public Knjiga findOne(Long id);
	public List<Knjiga> findAll();
	public int save (Knjiga knjiga);
	public int update (Knjiga knjiga);
	public int delete(Long id);
	public List<Knjiga> find(String naziv,Double cenaOd,Double cenaDo);
	

}
