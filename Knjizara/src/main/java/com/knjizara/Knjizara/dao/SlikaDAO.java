package com.knjizara.Knjizara.dao;

import java.util.ArrayList;
import java.util.List;

import com.knjizara.Knjizara.model.Slika;

public interface SlikaDAO {
	
	public Slika findOne(Long id);
	
	public List<Slika> findAll();
	
	public List<Slika> find(String naziv);
	
	public int save(Slika slika);
	
	public int [] save(ArrayList<Slika> slike);
	
	public int update(Slika slika);
	
	public int delete(Long id);
}
