package com.knjizara.Knjizara.service;

import java.util.List;

import com.knjizara.Knjizara.model.Komentar;



public interface KomentarService {
	Komentar findOne(Long id);
	
	List<Komentar> findAll(Long knjigaId);
	
	List<Komentar> findAll();
	
	Komentar save(Komentar komentar);
	
	Komentar update(Komentar komentar);
	

}
