package com.knjizara.Knjizara.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.knjizara.Knjizara.dao.KnjigaDAO;
import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Zanr;
import com.knjizara.Knjizara.service.KnjigaService;

@Service
public class DatabaseKnjigaService implements KnjigaService {

	
	@Autowired
	private KnjigaDAO knjigaDAO;
	
	public Knjiga findOne(Long id) {
		
		return knjigaDAO.findOne(id);
	}

	public List<Knjiga> findAll() {
		// TODO Auto-generated method stub
		return knjigaDAO.findAll();
	}

	public Knjiga save(Knjiga knjiga) {
		knjigaDAO.save(knjiga);
		return knjiga;
	}

	public List<Knjiga> save(List<Knjiga> knjige) {
		// TODO Auto-generated method stub
		return null;
	}

	public Knjiga update(Knjiga knjiga) {
		knjigaDAO.update(knjiga);
		return knjiga;
	}

	public List<Knjiga> update(List<Knjiga> knjige) {
		// TODO Auto-generated method stub
		return null;
	}

	public Knjiga delete(Long id) {
		Knjiga knjiga=findOne(id);
		if(knjiga!=null)
			knjigaDAO.delete(id);
		return knjiga;
	}

	public List<Knjiga> deleteAll(Knjiga knjiga) {
		// TODO Auto-generated method stub
		return null;
	}

	public void delete(List<Long> ids) {
		// TODO Auto-generated method stub
		
	}

	public List<Knjiga> find(String naziv,Double cenaOd,Double cenaDo) {
		List<Knjiga> knjige=knjigaDAO.find(naziv, cenaOd, cenaDo);
		System.out.println("Knjige iz database"+knjige);
		if(naziv==null) {
			naziv="";
		}
		
		if(cenaOd==null) {
			cenaOd=0.00;
		}
		if(cenaDo==null) {
			cenaDo=Double.MAX_VALUE;
		}
		
		List<Knjiga> rezultat=new ArrayList<>();
		for(Knjiga itKnjiga:knjige){
			if(!itKnjiga.getNaziv().toLowerCase().contains(naziv.toLowerCase())) {
				continue;
			}
			
			
			if((itKnjiga.getCena()>=cenaOd && itKnjiga.getCena()<=cenaDo)) {
				continue;
			}
			
			
			rezultat.add(itKnjiga);
		}
		System.out.println("Rezultat iz database"+rezultat);
		return rezultat;
				
	
	}

	public List<Knjiga> findBySlikaId(Long slikaId) {
		// TODO Auto-generated method stub
		return null;
	}


	
}
