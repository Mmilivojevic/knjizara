package com.knjizara.Knjizara.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.knjizara.Knjizara.dao.KorisnikDAO;
import com.knjizara.Knjizara.model.Korisnik;
import com.knjizara.Knjizara.service.KorisnikService;
@Service
public class DatabaseKorisnikService implements KorisnikService {
	
	@Autowired
	private KorisnikDAO korisnikDAO;
	
	public Korisnik findOne(String korisnickoIme) {
		return korisnikDAO.findOne(korisnickoIme);
	}

	public Korisnik findOne(String korisnickoIme, String lozinka) {
		
		return korisnikDAO.findOne(korisnickoIme, lozinka);
	}

	public List<Korisnik> findAll() {
		
		return korisnikDAO.findAll();
	}

	public Korisnik save(Korisnik korisnik) {
		 korisnikDAO.save(korisnik);
		return korisnik;
	}

	public List<Korisnik> save(List<Korisnik> korisnici) {
		// TODO Auto-generated method stub
		return null;
	}

	public Korisnik update(Korisnik korisnik) {
		korisnikDAO.update(korisnik);
		return korisnik;
	}

	public List<Korisnik> update(List<Korisnik> korisnici) {
		return null;
	}

	public Korisnik delete(String korisnickoIme) {
		Korisnik korisnik=findOne(korisnickoIme);
		if(korisnik!=null) {
			korisnikDAO.delete(korisnickoIme);
		}
		return korisnik;
	}

	public void delete(List<String> korisnickaImena) {
		// TODO Auto-generated method stub
		
	}

	public List<Korisnik> find(String korisnickoIme, String eMail, String ime, String prezime, String adresa,
			String broj, LocalDateTime datumRegistracije) {
		
		return korisnikDAO.findAll();
	}

	public List<Korisnik> findByKorisnickoIme(String korisnickoIme) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Korisnik update1(Korisnik korisnik) {
		korisnikDAO.update1(korisnik);
		return korisnik;
	}
	public Korisnik findOne1(String korisnickoIme) {
		return korisnikDAO.findOne1(korisnickoIme);
	}

	@Override
	public Korisnik findBlokiranogKorisnika(String korisnickoIme, String lozinka) {
		
		return korisnikDAO.findBlokiranogKorisnika(korisnickoIme, lozinka);
	}
	

}
