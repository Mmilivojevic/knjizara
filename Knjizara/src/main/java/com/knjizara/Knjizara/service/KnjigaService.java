package com.knjizara.Knjizara.service;

import java.util.List;

import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Korisnik;

public interface KnjigaService {
	Knjiga findOne(Long id);
	List<Knjiga> findAll();
	Knjiga save(Knjiga knjiga);
	List<Knjiga> save(List<Knjiga> knjige);
	Knjiga update (Knjiga knjiga);
	List<Knjiga> update(List<Knjiga> knjige);
	Knjiga delete(Long id);
	List<Knjiga> deleteAll(Knjiga knjiga);
	void delete(List<Long> ids);
	List<Knjiga> find(String naziv,Double cenaOd,Double cenaDo);
	List<Knjiga> findBySlikaId(Long slikaId);
	
	
}
