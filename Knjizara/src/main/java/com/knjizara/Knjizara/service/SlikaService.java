package com.knjizara.Knjizara.service;

import java.util.List;

import com.knjizara.Knjizara.model.Slika;

public interface SlikaService {
	Slika findOne(Long id);
	List<Slika> findAll();
	List<Slika> find(Long[] ids);
	Slika save(Slika slika);
	List<Slika> save(List<Slika> slika);
	Slika update(Slika slika);
	List<Slika> update(List<Slika> slike);
	Slika delete(Long id);
	void delete(List<Long> ids);
	List<Slika> find(String naziv);
}
