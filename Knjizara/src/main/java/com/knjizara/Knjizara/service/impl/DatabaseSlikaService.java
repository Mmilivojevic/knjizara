package com.knjizara.Knjizara.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.knjizara.Knjizara.dao.SlikaDAO;
import com.knjizara.Knjizara.model.Slika;
import com.knjizara.Knjizara.service.SlikaService;

@Service
public class DatabaseSlikaService implements SlikaService {

	@Autowired
	private SlikaDAO slikaDAO;
	
	@Override
	public Slika findOne(Long id) {
		
		return slikaDAO.findOne(id);
	}

	@Override
	public List<Slika> findAll() {
		
		return slikaDAO.findAll();
	}

	@Override
	public List<Slika> find(Long[] ids) {
		List<Slika> rezultat = new ArrayList<>();
		for (Long id: ids) {
			Slika slika = slikaDAO.findOne(id);
			rezultat.add(slika);
		}

		return rezultat;
	}

	@Override
	public Slika save(Slika slika) {
		slikaDAO.save(slika);
		return slika;
	}

	@Override
	public List<Slika> save(List<Slika> slika) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Slika update(Slika slika) {
		slikaDAO.update(slika);
		return slika;
	}

	@Override
	public List<Slika> update(List<Slika> slike) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Slika delete(Long id) {
		Slika slika=findOne(id);
		if(slika!=null) {
			slikaDAO.delete(id);
		}
		return slika;
	}

	@Override
	public void delete(List<Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Slika> find(String naziv) {
		if(naziv==null) {
			return slikaDAO.findAll();
			}
		return slikaDAO.find(naziv);
	}

}
