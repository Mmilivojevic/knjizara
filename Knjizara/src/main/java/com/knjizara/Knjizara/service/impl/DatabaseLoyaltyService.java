package com.knjizara.Knjizara.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.knjizara.Knjizara.dao.LoyaltyDAO;
import com.knjizara.Knjizara.model.LoyaltyKartica;
import com.knjizara.Knjizara.service.LoyaltyService;

@Service
public class DatabaseLoyaltyService implements LoyaltyService {

	@Autowired
	LoyaltyDAO loyaltyKarticaDAO;
	
	@Override
	public LoyaltyKartica findOne(Long id) {
		// TODO Auto-generated method stub
		return loyaltyKarticaDAO.findOne(id);
	}

	@Override
	public List<LoyaltyKartica> findAll(String korisnickoIme) {
		// TODO Auto-generated method stub
		return loyaltyKarticaDAO.findAll(korisnickoIme);
	}

	@Override
	public List<LoyaltyKartica> findAll() {
		// TODO Auto-generated method stub
		return loyaltyKarticaDAO.findAll();
	}

	@Override
	public LoyaltyKartica saveLoyaltyKartica(LoyaltyKartica loyaltyKartica) {
		// TODO Auto-generated method stub
		loyaltyKarticaDAO.saveLoyaltyKartica(loyaltyKartica);
		return loyaltyKartica;
	}

	@Override
	public LoyaltyKartica updateLoyaltyKartica(LoyaltyKartica loyaltyKartica) {
		// TODO Auto-generated method stub
		loyaltyKarticaDAO.updateLoyaltyKartica(loyaltyKartica);
		return loyaltyKartica;
	}

}
