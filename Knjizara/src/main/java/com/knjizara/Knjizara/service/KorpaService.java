package com.knjizara.Knjizara.service;

import java.util.List;

import com.knjizara.Knjizara.model.Korpa;


public interface KorpaService {
	
	Korpa findOne(Long korpaId);
	
	List<Korpa> findOne( String korisnickoIme, Long knjigaId);
	
	List<Korpa> findAll();
	
	List<Korpa> findAll(String korisnickoIme);
	
	List<Korpa> findAll2(String knjigaId);
	
	Korpa save(Korpa korpa);
	
	Korpa delete(Long korpaId);


}
