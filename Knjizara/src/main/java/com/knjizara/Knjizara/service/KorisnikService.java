package com.knjizara.Knjizara.service;

import java.time.LocalDateTime;
import java.util.List;

import com.knjizara.Knjizara.model.Korisnik;

public interface KorisnikService {
	Korisnik findOne(String korisnickoIme);
	Korisnik findOne(String korisnickoIme,String lozinka);
	List<Korisnik> findAll();
	Korisnik save(Korisnik korisnik);
	List<Korisnik> save(List<Korisnik> korisnici);
	Korisnik update(Korisnik korisnik);
	List<Korisnik> update(List<Korisnik> korisnici);
	Korisnik delete(String korisnickoIme);
	void delete(List<String> korisnickaImena);
	List<Korisnik> find(String korisnickoIme,String eMail,String ime,String prezime,String adresa,String broj,LocalDateTime datumRegistracije);
	List<Korisnik> findByKorisnickoIme(String korisnickoIme);
	Korisnik update1(Korisnik korisnik);
	 Korisnik findOne1(String korisnickoIme);
	 Korisnik findBlokiranogKorisnika(String korisnickoIme,String lozinka);
}
