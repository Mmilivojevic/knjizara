package com.knjizara.Knjizara.service;

import java.util.List;

import com.knjizara.Knjizara.model.LoyaltyKartica;

public interface LoyaltyService {
	LoyaltyKartica findOne(Long id);
	List<LoyaltyKartica> findAll(String korisnickoIme);
	List<LoyaltyKartica> findAll();
	LoyaltyKartica saveLoyaltyKartica(LoyaltyKartica loyaltyKartica);
	LoyaltyKartica updateLoyaltyKartica(LoyaltyKartica loyaltyKartica);

}
