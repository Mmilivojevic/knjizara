package com.knjizara.Knjizara.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.knjizara.Knjizara.dao.KorpaDAO;
import com.knjizara.Knjizara.model.Korpa;
import com.knjizara.Knjizara.service.KorpaService;

@Service
public class DatabaseKorpaService implements KorpaService {

	@Autowired
	private KorpaDAO korpaDAO;
	@Override
	public Korpa findOne(Long korpaId) {
		// TODO Auto-generated method stub
		return korpaDAO.findOne(korpaId);
	}

	@Override
	public List<Korpa> findOne(String korisnickoIme, Long knjigaId) {
		// TODO Auto-generated method stub
		return korpaDAO.findOne(korisnickoIme, knjigaId);
	}

	@Override
	public List<Korpa> findAll() {
		// TODO Auto-generated method stub
		return korpaDAO.findAll();
	}

	@Override
	public List<Korpa> findAll(String korisnickoIme) {
		// TODO Auto-generated method stub
		return korpaDAO.findAll(korisnickoIme);
	}

	@Override
	public List<Korpa> findAll2(String knjigaId) {
		// TODO Auto-generated method stub
		return korpaDAO.findAll2(knjigaId);
	}

	@Override
	public Korpa save(Korpa korpa) {
		// TODO Auto-generated method stub
		korpaDAO.save(korpa);
		return korpa ;
	}

	@Override
	public Korpa delete(Long korpaId) {
		// TODO Auto-generated method stub
		Korpa korpa=findOne(korpaId);
		if(korpa != null) {
			korpaDAO.delete(korpaId);
		}
		return korpa;
	}

}
