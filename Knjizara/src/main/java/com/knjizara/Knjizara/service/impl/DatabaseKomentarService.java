package com.knjizara.Knjizara.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.knjizara.Knjizara.dao.KomentarDAO;
import com.knjizara.Knjizara.model.Komentar;
import com.knjizara.Knjizara.service.KomentarService;

@Service
public class DatabaseKomentarService implements KomentarService{

	@Autowired
	private KomentarDAO komentarDAO;
	@Override
	public Komentar findOne(Long id) {
		// TODO Auto-generated method stub
		return komentarDAO.findOne(id);
	}

	@Override
	public List<Komentar> findAll(Long knjigaId) {
		// TODO Auto-generated method stub
		return komentarDAO.findAll(knjigaId);
	}

	@Override
	public List<Komentar> findAll() {
		// TODO Auto-generated method stub
		return komentarDAO.findAll();
	}

	@Override
	public Komentar save(Komentar komentar) {
		// TODO Auto-generated method stub
		komentarDAO.save(komentar);
		return komentar;
	}

	@Override
	public Komentar update(Komentar komentar) {
		// TODO Auto-generated method stub
		komentarDAO.update(komentar);
		return komentar;
	}

}
