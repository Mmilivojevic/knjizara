package com.knjizara.Knjizara.controller;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Komentar;
import com.knjizara.Knjizara.model.Korisnik;
import com.knjizara.Knjizara.model.LoyaltyKartica;
import com.knjizara.Knjizara.service.KorisnikService;
import com.knjizara.Knjizara.service.LoyaltyService;

@Controller
@RequestMapping(value="/Korisnici")
public class KorisnikController {
	public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	
	public static final String KORISNIK_KEY="prijavljeniKorisnik";
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	LoyaltyService loyaltyService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	@PostConstruct
	public void init() {
		baseURL=servletContext.getContextPath()+"/";
	}
	
	@GetMapping
	public ModelAndView getKorisnici(HttpSession session,HttpServletResponse response) {
		List<Korisnik> korisnici=korisnikService.findAll();
		ModelAndView rezultat=new ModelAndView();
		rezultat.addObject("korisnici",korisnici);
		return rezultat;
		
	}
	@PostMapping(value="/Register")
	public ModelAndView register(@RequestParam String korisnickoIme,@RequestParam String lozinka,@RequestParam String ponovljenaLozinka,@RequestParam String eMail,@RequestParam String ime,@RequestParam String prezime,@RequestParam String adresa,@RequestParam String broj,@RequestParam Date datumRodjenja,HttpSession session,HttpServletResponse response) {
		try {
			Korisnik postojeciKorisnik=korisnikService.findOne(korisnickoIme);
			if(postojeciKorisnik !=null) {
				throw new Exception("Korisnicko ime vec postoji!");
			}
			if(korisnickoIme.equals("") || lozinka.equals("")) {
				throw new Exception("Korisnicko ime i lozinka ne smeju biti prazni");
			}
			if(!lozinka.equals(ponovljenaLozinka)) {
				throw new Exception("Lozinka i ponovljena lozinka se ne podudaraju!");
			}if(eMail.equals("")) {
				throw new Exception("E-mail ne sme biti prazan");
			}if(ime.equals("")) {
				throw new Exception("Ime ne sme biti prazno");
			}if(prezime.equals("")) {
				throw new Exception("Prezime ne sme biti prazno");
			}if(adresa.equals("")) {
				throw new Exception("Adresa ne sme biti prazna");
			}if(broj.equals("")) {
				throw new Exception("Broj ne sme biti prazan");
			}
			System.out.println("Datum rodjenja je :" +datumRodjenja);

			
			LocalDateTime datumRegistracije=LocalDateTime.now();
			datumRegistracije.format(formatter);
			System.out.println("Datum registracije" + datumRegistracije);
			Korisnik korisnik=new Korisnik(korisnickoIme,lozinka,eMail,ime,prezime,adresa,broj,datumRegistracije, datumRodjenja);
			korisnikService.save(korisnik);
			
			response.sendRedirect(baseURL+"prijava.html");
			return null;
		}catch(Exception ex) {
			//ispisivanje greske
			ex.printStackTrace();
			String poruka=ex.getMessage();
			if(poruka=="") {
				poruka="Neuspesna registracije";
			}
			
			ModelAndView rezultat=new ModelAndView("registracija");
			rezultat.addObject("poruka",poruka);
			return rezultat;
		}
	}
	@PostMapping(value="/Edit")
	public void izmenaKorisnika(@RequestParam String korisnickoIme, @RequestParam (required=false) boolean administrator, @RequestParam(required=false) boolean block,
					  HttpSession session, HttpServletResponse response) throws IOException {
		
		
		
		
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (loggedInUser == null || !loggedInUser.isAdministrator()) {
			response.sendRedirect(baseURL + "Users");
			return;
		}
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		
		if (korisnik.isAdministrator()) {
		
		 
		
			korisnik.setAdministrator(administrator);
			korisnikService.update(korisnik);
			System.out.println("USLO OVDE");
			
		}
		else {
			
			korisnik.setBlock(block);
			korisnikService.update(korisnik);
			System.out.println("USLO OVDE 1111");
		
		}
		
		System.out.println(block);
		System.out.println(administrator);

		System.out.println("Korisnik" +korisnik);
		
		
		
		
		response.sendRedirect(baseURL + "Korisnici");
	}
	
	
	@GetMapping(value="/Details")
	public ModelAndView getKorisnik(@RequestParam String korisnickoIme,HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		// samo administrator može da vidi druge korisnike; svaki korisnik može da vidi sebe
		if (prijavljeniKorisnik == null || (!prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.getKorisnickoIme().equals(korisnickoIme))) {
			response.sendRedirect(baseURL + "Korisnici");
			return null;
		}

		// validacija
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		System.out.println("Korisnik find one"+ korisnik);
		if (korisnik == null) {
			response.sendRedirect(baseURL + "Korisnici");
			return null;
		}

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("korisnik");
		rezultat.addObject("korisnik", korisnik);

		return rezultat;

	}
	
	@SuppressWarnings("null")
	@PostMapping(value="/Login")
	public ModelAndView postLogin(@RequestParam String korisnickoIme,@RequestParam String lozinka,HttpSession session,HttpServletResponse response) {
		try {
			//validacija
			Korisnik korisnik=korisnikService.findBlokiranogKorisnika(korisnickoIme, lozinka);
			if(korisnik==null && korisnik.isBlock()==false) {
				throw new Exception("Neispravno korisnicko ime ili lozinka");
				
			}
			//prijava
			session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);
			response.sendRedirect(baseURL+"Knjige");
			return null;
		}catch(Exception ex) {
			//ispis greske
			String poruka=ex.getMessage();
			if(poruka=="") {
				poruka="Neuspesna prijava!";
				
			}
			//prosledjivanje
			ModelAndView rezultat=new ModelAndView("prijava");
			rezultat.addObject("poruka",poruka);
			return rezultat;
			
		}
	}
	
	@PostMapping(value="/DodavanjeLoyaltyKartice")
	@ResponseBody
	public Map<String, Object> create(@RequestParam String korisnickoIme,HttpSession session, HttpServletRequest request,HttpServletResponse response) throws IOException {
	
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (loggedInUser == null || loggedInUser.isAdministrator()) {
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "odbijen");
			return odgovor;
		}

		try {
		
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		System.out.println(korisnik);
		
		
	
			
			boolean status=false;
			int brojPoena=4;
			String popust="20%";
			
			
			
			LoyaltyKartica lk= new LoyaltyKartica(popust="20%", brojPoena=4, status=false, korisnik);
			loyaltyService.saveLoyaltyKartica(lk);
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "ok");
			response.sendRedirect(baseURL+"Knjige");
			return odgovor;
			
			
		} catch (Exception ex) {
	
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neupesno!";
			}
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", poruka);
			return odgovor;
		}
		
		
	}
	
	
	@GetMapping(value="/Profil")
	public ModelAndView profilKorisnika(@RequestParam String korisnickoIme, 
			HttpSession session, HttpServletResponse response) throws IOException {		
		
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		
		
		if (loggedInUser == null || (!loggedInUser.getKorisnickoIme().equals(korisnickoIme))) {
			response.sendRedirect(baseURL);
			return null;
		}

	
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		if (korisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}

	
		ModelAndView rezultat = new ModelAndView("profil");
		rezultat.addObject("korisnik", korisnik);

		return rezultat;
	}
	@GetMapping(value="/Logout")
	public void logout( HttpServletResponse response,HttpServletRequest request) throws IOException {
			
		

		
		request.getSession(true).removeAttribute("prijavljeniKorisnik");
		
		
		response.sendRedirect(baseURL+"/prijava.html");
		
	}

	
}
