package com.knjizara.Knjizara.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;


import com.knjizara.Knjizara.model.Komentar;
import com.knjizara.Knjizara.service.KomentarService;

@Controller
@RequestMapping(value="/Komentari")
public class KomentarController implements ServletContextAware{

	@Autowired
	private KomentarService komentarService;
	@Autowired 
	private ServletContext servletContext;
	
	private String baseURL;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	@PostConstruct
	public void init() {
		baseURL=servletContext.getContextPath()+"/";
	}
	@GetMapping
	public ModelAndView index(
			HttpSession session, HttpServletResponse response) throws IOException {
		
		List<Komentar> komentariAdmin = komentarService.findAll();

	
		ModelAndView rezultat=new ModelAndView("komentariAdmin");

		rezultat.addObject("komentariAdmin", komentariAdmin);
		return rezultat;
	}

	@GetMapping(value="/KomentarAdmin")
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ModelAndView komentar() throws IOException {

	
		ModelAndView rezultat = new ModelAndView("komentarAdmin");
		return rezultat;
		
	}
	
	@GetMapping(value="/JedanKomentarAdminDetalji")
	@ResponseBody
	@SuppressWarnings("unchecked")
	public Map<String, Object> komentarAdminDetalji(@RequestParam Long id, 
			HttpSession session) throws IOException {
		
		Komentar komentar = komentarService.findOne(id);


		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("komentar", komentar);
		return odgovor;
	}
	
	
	@PostMapping(value="/EditKomentaraAdmin")
	@ResponseBody
	public Map<String, Object> izmeniStatusKomentara(@RequestParam Long id, @RequestParam Boolean status,
			
			HttpSession session, HttpServletRequest request) throws IOException {
		
		
		try {
		
			Komentar komentar = komentarService.findOne(id);
			
			if (komentar == null) {
				throw new Exception("Ne postojiii");
			}	
			if (status.equals("")) {
				throw new Exception("Ne sme biti prazno polje");
			}
			

			komentar.setStatus(status);
			
			komentarService.update(komentar);
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "ok");	
			return odgovor;
			
		} catch (Exception ex) {
	
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Failed";
			}
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", poruka);
			return odgovor;
		}
	}
	

}
