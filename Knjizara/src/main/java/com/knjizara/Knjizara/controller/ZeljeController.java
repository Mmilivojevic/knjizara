package com.knjizara.Knjizara.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Korisnik;
import com.knjizara.Knjizara.model.Korpa;
import com.knjizara.Knjizara.service.KnjigaService;
import com.knjizara.Knjizara.service.KorpaService;

@Controller
@RequestMapping(value="/Zelje")
public class ZeljeController {
	
	public static final String IZABRANE_KNJIGE_KORISNIK_KEY = "izabraneKnjigeKorisnik";
	
private  String bURL; 
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private KorpaService korpaService;
	
	@PostConstruct
	public void init() {	
		
			bURL = servletContext.getContextPath()+"/";			
	}
	

	
	@GetMapping
	public ModelAndView nadjiZelje(
			@RequestParam String korisnickoIme, 
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		
		
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KorisnikController.KORISNIK_KEY);
		
		if(korisnickoIme == null || korisnik.isAdministrator() == true) {
			response.sendRedirect(bURL+"Knjige");
		}
		
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		
		if (loggedInUser == null || (!loggedInUser.getKorisnickoIme().equals(korisnickoIme))) {
			response.sendRedirect(bURL+"/Knjige");
			return null;
		}
		
		
		List<Korpa> korpa = korpaService.findAll(korisnickoIme);
		
		
		// prosleđivanje
		
		ModelAndView rezultat = new ModelAndView("zelje");
		rezultat.addObject("korpaZaKorisnika", korpa);
		
		
		
		return rezultat;
	}
	
	
	@PostMapping
	public ModelAndView dodajKnjiguUZelje(
			
			@RequestParam String knjigaId,
			@RequestParam String korisnickoIme,
			
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
	
		
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KorisnikController.KORISNIK_KEY);		
		if(korisnik==null || korisnik.isAdministrator()==true) {
			response.sendRedirect(bURL+"/Knjige");
		}
		
		Long id = new Long(knjigaId);
		
		if(knjigaId!=null && id<=0) {
			response.sendRedirect(bURL+"/Knjige");
			return null;
		}
		
		Knjiga knjiga = knjigaService.findOne(id);
		if(knjiga==null) {
			response.sendRedirect(bURL+"/Knjige");
			return null;
		}
		
		Korpa korpa = new Korpa(korisnik, knjiga);
		korpaService.save(korpa);
		
		List<Korpa> korpaZaKorisnika = korpaService.findAll(korisnickoIme);

		// prosleđivanje
		
		ModelAndView rezultat = new ModelAndView("zelje");
		rezultat.addObject("korpaZaKorisnika", korpaZaKorisnika);

		return rezultat;
	}
	
	@PostMapping(value="/Obrisi")
	public ModelAndView obrisiZelju(@RequestParam Long korpaId, 	@RequestParam String korisnickoIme,
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
	
		// čitanje
	
			
		Korpa korpa = korpaService.findOne(korpaId);
		if(korpa==null) {
			
			System.out.println("asasdasdasdasdas");
			response.sendRedirect(bURL+"/Knjige");
			
			return null;
		}
		
		korpaService.delete(korpaId);
			
		
		
		String korisnickoIme2=korisnickoIme.split(",")[0];
		System.out.println("KORISNICKO IME"+korisnickoIme);
		List<Korpa> korpa2 = korpaService.findAll(korisnickoIme2);
//	 System.out.println("Korpa 2 posle brisanja"+ korpa2);
//		
		ModelAndView rezultat = new ModelAndView("zelje");
		rezultat.addObject("korpaZaKorisnika", korpa2);
		
		
		
		return rezultat;
	}

			

	

}
