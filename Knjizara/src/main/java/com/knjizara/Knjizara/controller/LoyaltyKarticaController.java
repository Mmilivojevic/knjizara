package com.knjizara.Knjizara.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.knjizara.Knjizara.model.Komentar;
import com.knjizara.Knjizara.model.LoyaltyKartica;
import com.knjizara.Knjizara.service.LoyaltyService;

@Controller
@RequestMapping(value="/LoyaltiKartica")
public class LoyaltyKarticaController implements ServletContextAware {
	
	@Autowired
	private LoyaltyService loyaltyService;
	
	@Autowired
	private ServletContext servletContext;
	
	private String baseURL;

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext=servletContext;
		
	}
	
	@PostConstruct
	public void init() {
		baseURL=servletContext.getContextPath()+"/";
	}
	@GetMapping
	public ModelAndView index(HttpSession session, HttpServletResponse response) throws IOException {
		
		List<LoyaltyKartica> loyaltyAdmin = loyaltyService.findAll();

	
		ModelAndView rezultat=new ModelAndView("loyaltyAdmin");

		rezultat.addObject("loyaltyAdmin", loyaltyAdmin);
		return rezultat;
	}
	
	@GetMapping(value="/LoyaltyKarticaAdmin")
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ModelAndView loyaltyKartica() throws IOException {

	
		ModelAndView rezultat = new ModelAndView("loyaltyAdmin");
		return rezultat;
		
	}
	
	@GetMapping(value="/JednaLoyaltyKarticaAdminDetalji")
	@ResponseBody
	@SuppressWarnings("unchecked")
	public Map<String, Object> komentarAdminDetalji(@RequestParam Long id, HttpSession session) throws IOException {
		
		LoyaltyKartica kartica = loyaltyService.findOne(id);


		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("kartica", kartica);
		return odgovor;
	}
	
	
	@PostMapping(value="/EditLoyaltyKarticeAdmin")
	@ResponseBody
	public Map<String, Object> izmeniStatusKartice(@RequestParam Long id, @RequestParam Boolean status,
			
			HttpSession session, HttpServletRequest request) throws IOException {
		
		
		try {
		
			LoyaltyKartica lKartica = loyaltyService.findOne(id);
			
			if (lKartica == null) {
				throw new Exception("Ne postojiii");
			}	
			if (status.equals("")) {
				throw new Exception("Ne sme biti prazno polje");
			}
			

			lKartica.setStatus(status);
			
			loyaltyService.updateLoyaltyKartica(lKartica);
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "ok");	
			return odgovor;
			
		} catch (Exception ex) {
	
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Failed";
			}
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", poruka);
			return odgovor;
		}
	}
	
	
	

}
