package com.knjizara.Knjizara.controller;



import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.ModelAndView;


import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.model.Komentar;
import com.knjizara.Knjizara.model.Korisnik;
import com.knjizara.Knjizara.model.Slika;
import com.knjizara.Knjizara.model.Zanr;
import com.knjizara.Knjizara.service.KnjigaService;
import com.knjizara.Knjizara.service.KomentarService;
import com.knjizara.Knjizara.service.KorisnikService;
import com.knjizara.Knjizara.service.SlikaService;
import com.knjizara.Knjizara.service.ZanrService;

@Controller
@RequestMapping(value="/Knjige")
public class KnjigaController implements ServletContextAware {

	@Autowired 
	private KnjigaService knjigaService;

	@Autowired
	private SlikaService slikaService;
	@Autowired 
	private ZanrService zanrService;
	@Autowired
	private KomentarService komentarService;
	@Autowired
	private KorisnikService korisnikService;
	@Autowired 
	private ServletContext servletContext;
	private String baseURL;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	@PostConstruct
	public void init() {
		baseURL=servletContext.getContextPath()+"/";
	}
	
	@GetMapping
//	@RequestParam(required=false) String naziv,@RequestParam(required=false) Long zanrId,@RequestParam(required=false) double cenaOd,@RequestParam(required=false) double cenaDo,@RequestParam(required=false) String autor,@RequestParam(required=false) String jezik,
	public ModelAndView index(
//			@RequestParam(required=false) String naziv,
//			@RequestParam(required=false) Double cenaOd,
//			@RequestParam(required=false) Double cenaDo,
			HttpSession session, HttpServletResponse response
			) throws IOException{
		
//		if(naziv!=null&& naziv.trim().equals("")) {
//			naziv=null;}
//		if(autor!=null &&autor.trim().equals("")) {
//			autor=null;
//		}
//		if(jezik!=null &&jezik.trim().equals("")) {
//			jezik=null;
//		}
		
		List<Knjiga> knjige=knjigaService.findAll();
//		List<Knjiga> knjige=knjigaService.find(naziv, cenaOd, cenaDo);
//		List<Zanr> zanrovi=zanrService.findAll();
		List<Slika> imagePath=slikaService.findAll();
		ModelAndView rezultat=new ModelAndView("knjige");
		System.out.println("Knjige"+knjige);
		rezultat.addObject("knjige",knjige);
		rezultat.addObject("imagePath",imagePath);
//		rezultat.addObject("zanrovi",zanrovi);
		System.out.println("slike"+imagePath);
		
		return rezultat;
		
		}
	
	@GetMapping("/Details")
	public ModelAndView details(@RequestParam Long id ,HttpSession session) {
		Knjiga knjiga=knjigaService.findOne(id);
		
		List<Slika> slike=slikaService.findAll();
		List<Zanr> zanrovi=zanrService.findAll();
		ModelAndView rezultat=new ModelAndView("knjiga");
		rezultat.addObject("knjiga",knjiga);
		rezultat.addObject("slike",slike);
		rezultat.addObject("zanrovi",zanrovi);
		
		System.out.println("Slike  "+slike);
		System.out.println("Knjiga  "+knjiga);
		System.out.println("Zanrovi  "+zanrovi);
		
		return rezultat;
		
	}
	@PostMapping("/Edit")
	public void edit(@RequestParam Long id,@RequestParam String naziv,@RequestParam String izdavackaKuca,@RequestParam String autori,@RequestParam int godinaIzdavanja,@RequestParam String opis,@RequestParam(name="slikaId",required=false) Long[] imageId,@RequestParam(name="zanrId", required=false) Long[] zanrIds,@RequestParam double cena,@RequestParam int brojStranica,@RequestParam String tipPoveza,@RequestParam String pismo,@RequestParam String jezik,@RequestParam Integer brojKnjiga,HttpSession session,HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Knjige");
			return;
		}
		Knjiga knjiga=knjigaService.findOne(id);
		if(knjiga==null) {
			response.sendRedirect(baseURL+"Knjige");
			return;
		}
		 //ISBN==0||ISBN<13
		if(naziv==null ||naziv.equals("") ||izdavackaKuca==null||izdavackaKuca.equals("")||autori==null||autori.equals("")||godinaIzdavanja==0|| godinaIzdavanja<4 || opis==null ||opis.equals("")||cena==0||cena<5||brojStranica==0||brojStranica<2||tipPoveza==null||tipPoveza.equals("")||pismo==null||pismo.equals("")||jezik==null||jezik.equals("")) {
			response.sendRedirect(baseURL+"Knjige/Details?id="+id);
			
		}
		
		//izmena
		knjiga.setNaziv(naziv);
		System.out.println("Naziv "+naziv);
		knjiga.getISBN();
		knjiga.setIzdavackaKuca(izdavackaKuca);
		System.out.println("Izdavacka kuca "+izdavackaKuca);
		knjiga.setAutori(autori);
		System.out.println("Autor "+autori);
		knjiga.setGodinaIzdavanja(godinaIzdavanja);
		knjiga.setKratakOpis(opis);
		System.out.println("Opis "+opis);
		knjiga.setImagePath(slikaService.find(imageId));
		System.out.println("Slika  "+imageId);
		knjiga.setZanrovi(zanrService.find(zanrIds));
		System.out.println("Zanr  "+zanrIds);
		knjiga.setCena(cena);
		System.out.println("Cena  "+cena);
		knjiga.setBrojStranica(brojStranica);
		System.out.println("Broj stranica "+brojStranica);
		knjiga.setTipPoveza(tipPoveza);
		System.out.println("Tip poveza  "+tipPoveza);
		knjiga.setPismo(pismo);
		System.out.println("Pismo  "+pismo);
		knjiga.setJezik(jezik);
		System.out.println("Jezik  "+jezik);
		knjiga.setBrojKnjiga(brojKnjiga);
		
		knjigaService.update(knjiga);
		
		response.sendRedirect(baseURL+"Knjige");
	}
	@GetMapping(value="/Create")
	public ModelAndView create(HttpSession session,HttpServletResponse response) throws IOException {
		//autentifikacija i autorizacija pristupa stranici
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Knjige");
			return null;
		}
		//citanje
		List<Zanr> zanrovi=zanrService.findAll();
		List<Slika> slike=slikaService.findAll();
		//prosledjivanje na html
		ModelAndView rezultat=new ModelAndView("dodavanjeKnjige");
		rezultat.addObject("zanrovi",zanrovi);
		rezultat.addObject("slike",slike);
		return rezultat;
	}
	
	@PostMapping("/Create")
	public void create(@RequestParam String naziv,@RequestParam Long ISBN,@RequestParam String izdavackaKuca,@RequestParam String autori,@RequestParam int godinaIzdavanja,@RequestParam String kratakOpis,@RequestParam(name="slikaId", required=false) Long[] imageId,@RequestParam(name="zanrId", required=false) Long[] zanrIds,@RequestParam double cena,@RequestParam int brojStranica,@RequestParam String tipPoveza,@RequestParam String pismo,@RequestParam String jezik,HttpSession session,HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik=(Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik==null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL+"Knjige");
		}
//		try {
//		if (naziv.equals("")|| naziv==null ) {
//			throw new Exception("Naziv ne sme biti prazan!");
//		}
//		if (ISBN<13||ISBN==0) {
//			throw new Exception("ISBN mora da ima 13 cifara");
//		}
//		if (izdavackaKuca.equals("")||izdavackaKuca==null) {
//			throw new Exception("Izdavacka kuca ne sme biti prazna");
//		}
//		if (autori.equals("")||autori==null) {
//			throw new Exception("Autori ne smeju biti prazni");
//		}
//		if (godinaIzdavanja==0||godinaIzdavanja<4) {
//			throw new Exception("Godina izdavanja ne sme biti prazna");
//		}
//		if (kratakOpis.equals("")||kratakOpis==null) {
//			throw new Exception("Kratak opis ne sme biti prazna");
//		}
//		if (imageId==null||imageId.equals("")) {
//			throw new Exception("Morate izabrati sliku");
//		}
//		if (zanrIds==null||zanrIds.equals("")) {
//			throw new Exception("Morate izabrati zanr/ove");
//		}
//		if (cena==0||cena<4) {
//			throw new Exception("Cena ne sme biti prazna");
//		}
//		if (brojStranica==0) {
//			throw new Exception("Broj stranica ne sme biti prazna");
//		}
//		
//		if (!tipPoveza.equals("meki") && !tipPoveza.equals("tvrdi")) {
//			throw new Exception("Morate odabrati tip poveza!");
//		}
//		if (!pismo.equals("latinica") && !pismo.equals("cirilica")) {
//			throw new Exception("Morate odabrati pismo!");
//		}
//		if (jezik.equals("")||jezik==null) {
//			throw new Exception("Jezik ne sme biti prazan");
//		}
//		}
//		
//		
//		catch(Exception e){
//			String poruka=e.getMessage();
//			if(poruka=="") {
//				poruka="Neuspesno kreiranje knjige";
//			}
//			//prosledjivanje u modelAndView
//			ModelAndView rezultat=new ModelAndView("dodavanjeKnjige");
//			rezultat.addObject("poruka",poruka);
//			return;
//			
//		}
		//kreiranje
		Knjiga knjiga=new Knjiga(naziv,ISBN,izdavackaKuca,autori,godinaIzdavanja,kratakOpis,cena,brojStranica,tipPoveza,pismo,jezik);
		System.out.println("Knjiga "+knjiga);
		knjiga.setImagePath(slikaService.find(imageId));
		knjiga.setZanrovi(zanrService.find(zanrIds));
		knjigaService.save(knjiga);
		response.sendRedirect(baseURL+"Knjige");
	}
	@GetMapping(value="/SviKomentari")
	@ResponseBody
	@SuppressWarnings("unchecked")
	public Map<String, Object> sviKomentaru(@RequestParam Long knjigaId, 
			HttpSession session) throws IOException {
	
		
		List<Komentar> komentari = komentarService.findAll(knjigaId);
		System.out.println("Svi komentari" + komentari);

		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("komentari", komentari);
		return odgovor;
	}
	
	
	
	@PostMapping(value="/DodavanjeKomentara")
	@ResponseBody
	public Map<String, Object> create(@RequestParam String tekst, @RequestParam String ocena, @RequestParam Long komentarisanaKnjiga, @RequestParam String autorKomentara,  
			HttpSession session, HttpServletRequest request,HttpServletResponse response) throws IOException {
	
		Korisnik loggedInUser = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (loggedInUser == null || loggedInUser.isAdministrator()) {
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "odbijen");
			return odgovor;
		}

		Knjiga komentarisanaKnjiga1 = knjigaService.findOne(komentarisanaKnjiga);
		
		Korisnik autorKomentara1 = korisnikService.findOne(autorKomentara);
		
		
		long yourmilliseconds = System.currentTimeMillis();
		
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");    
	
		Date datumKomentara = new Date(yourmilliseconds);
		
		System.out.println(sdf.format(datumKomentara));
		
		
		try {
			

			if (tekst == null || tekst.equals("")) {
				throw new Exception("Polje za tekst ne sme biti prazno");
						
			}
			if (ocena == null || ocena.equals("")) {
				throw new Exception("Polje za ocenu ne sme biti prazno");
				
			}


			
			boolean status=false;
			
			Komentar komentar = new Komentar(tekst, ocena, datumKomentara,autorKomentara1,komentarisanaKnjiga1,status=false);
			komentarService.save(komentar);
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "ok");
			response.sendRedirect(baseURL+"Knjige");
			return odgovor;
			
			
		} catch (Exception ex) {
	
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neupesno!";
			}
			
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", poruka);
			return odgovor;
		}
		
		
	}
	@PostMapping(value="/Delete")
	public void delete(@RequestParam Long id, 
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}

		// brisanje
		knjigaService.delete(id);
	
		response.sendRedirect(baseURL + "/Knjige");
	}


}
