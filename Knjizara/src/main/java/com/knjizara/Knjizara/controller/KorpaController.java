package com.knjizara.Knjizara.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


import com.knjizara.Knjizara.model.Knjiga;
import com.knjizara.Knjizara.service.KnjigaService;


@RestController

@RequestMapping(value="/Korpa")
public class KorpaController {
	
	public static final String IZABRANE_KNJIGE_KORISNIK_KEY = "izabraneKnjigeKorisnik";
	
	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";			
	}
	
	@GetMapping
	public ModelAndView index(
			
			HttpSession session, HttpServletResponse response) throws IOException {		
	
			ModelAndView rezultat = new ModelAndView("korpa");
			
			return rezultat;
		
	}
	@PostMapping(value="/Details")
	@SuppressWarnings("unchecked")
	public ModelAndView detaljiKorpa(@RequestParam Long id, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Knjiga knjiga = knjigaService.findOne(id);

		System.out.println("Knjiga detalji korpa" + knjiga);

		List<Knjiga> odabrana = (List<Knjiga>) session.getAttribute(KorpaController.IZABRANE_KNJIGE_KORISNIK_KEY);
		if (!odabrana.contains(knjiga)) {
			odabrana.add(knjiga);

		}
		System.out.println("Odbabrana" + odabrana);
		
		ModelAndView rezultat = new ModelAndView("korpa");

		
		return rezultat;
	}
	

	@PostMapping(value="/Delete")
	@SuppressWarnings("unchecked")
	public ModelAndView obrisiKnjiguIzKorpe(@RequestParam Long id, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {

		Knjiga knjiga = knjigaService.findOne(id);
		
		List<Knjiga> odabrana = (List<Knjiga>) session.getAttribute(KorpaController.IZABRANE_KNJIGE_KORISNIK_KEY);
		if(odabrana.contains(knjiga)) {
			odabrana.remove(knjiga);     
	      }
		
		ModelAndView rezultat = new ModelAndView("korpa");
		
		return rezultat;
	}
	
	
}
